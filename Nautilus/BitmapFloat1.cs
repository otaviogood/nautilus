﻿#region ApacheLicense2.0
// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion
#define BITMAPFLOAT1

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Otavio.Math;
using System.Drawing;
using System.IO.Compression;
using System.IO;

namespace Otavio.Image
{
	public class BitmapFloat1
	{
		private int m_width, m_height;
		public int Width
		{
			get { return m_width; }
			set { m_width = value; }
		}
		public int Height
		{
			get { return m_height; }
			set { m_height = value; }
		}

		public float[] m_color;

		public BitmapFloat1(int width, int height)
		{
			ResetSize(width, height);
		}

#if CUDA_INTERFACE_F
		public BitmapFloat1(CudaInterface.CudaImageF cImage)
		{
			ResetSize(cImage.GetWidth(), cImage.GetHeight());
			unsafe
			{
				fixed (float* tempP = &m_color[0])
				{
					cImage.GetAsFloatArray(tempP);
				}
			}
		}
#endif

		public void ResetSize(int w, int h)
		{
			Height = h;
			Width = w;
			m_color = new float[Width * Height];
		}

		public BitmapFloat1 Copy()
		{
			BitmapFloat1 result = new BitmapFloat1(Width, Height);
			Array.Copy(m_color, result.m_color, m_color.Length);
			return result;
		}

		bool IsInBounds(int x, int y)
		{
			if (x < 0) return false;
			if (y < 0) return false;
			if (x >= Width) return false;
			if (y >= Height) return false;
			return true;
		}

		public float GetPixel(int x, int y)
		{
			return m_color[x + y * m_width];
		}

		public float GetPixelSafe(int x, int y)
		{
			if (x >= Width) x = Width - 1;
			if (x < 0) x = 0;
			if (y >= Height) y = Height - 1;
			if (y < 0) y = 0;
			return m_color[x + y * m_width];
		}

		public float GetPixelWrap(int x, int y)
		{
			x = (x+m_width) % m_width;
			y = (y+m_height) % m_height;
			return m_color[x + y * m_width];
		}

		public float GetPixelZeroEdge(int x, int y)
		{
			if (x >= Width) return 0;
			if (x < 0) return 0;
			if (y >= Height) return 0;
			if (y < 0) return 0;
			return m_color[x + y * m_width];
		}

		public float GetPixelBilinear(float2 pos)
		{
			//if (pos.x >= Width) pos.x = Width - 1;
			//if (pos.x < 0) pos.x = 0;
			//if (pos.y >= Height) pos.y = Height - 1;
			//if (pos.y < 0) pos.y = 0;
			float2 floor = new float2((float)System.Math.Floor(pos.x), (float)System.Math.Floor(pos.y));
			float2 remainder = pos - floor;
			float pix00 = GetPixelZeroEdge((int)floor.x, (int)floor.y);
			float pix10 = GetPixelZeroEdge((int)floor.x + 1, (int)floor.y);
			float pix01 = GetPixelZeroEdge((int)floor.x, (int)floor.y + 1);
			float pix11 = GetPixelZeroEdge((int)floor.x + 1, (int)floor.y + 1);

			float pixX0 = pix10 * remainder.x + pix00 * (1.0f - remainder.x);
			float pixX1 = pix11 * remainder.x + pix01 * (1.0f - remainder.x);
			float pixY = pixX1 * remainder.y + pixX0 * (1.0f - remainder.y);
			return pixY;
		}

		public void SetPixel(int x, int y, float pix)
		{
			m_color[x + y * Width] = pix;
		}

		public void SetPixelSafe(int x, int y, float pix)
		{
			if (x >= Width) return;
			if (x < 0) return;
			if (y >= Height) return;
			if (y < 0) return;
			m_color[x + y * Width] = pix;
		}

		public void AddPixel(int x, int y, float pix)
		{
			m_color[x + y * Width] += pix;
		}

		public System.Drawing.Bitmap DrawToBitmap()
		{
			if (m_color == null) return null;

			System.Drawing.Bitmap finalColorBitmap = null;
			if (finalColorBitmap == null) finalColorBitmap = new System.Drawing.Bitmap(Width, Height);
			System.Drawing.Imaging.PixelFormat pf = System.Drawing.Imaging.PixelFormat.Format24bppRgb;
			//if (UseAlpha) pf = System.Drawing.Imaging.PixelFormat.Format32bppArgb;
			// GDI+ still lies to us - the return format is BGR, NOT RGB.
			System.Drawing.Imaging.BitmapData bmData = finalColorBitmap.LockBits(
				new System.Drawing.Rectangle(0, 0, Width, Height),
				System.Drawing.Imaging.ImageLockMode.ReadWrite, pf);
			int stride = bmData.Stride;
			System.IntPtr Scan0 = bmData.Scan0;
			unsafe
			{
				//byte* p = (byte*)(void*)Scan0;
				int nOffset = stride - Width * 3;
				//if (UseAlpha) nOffset = stride - Width * 4;
				//for (int y = 0; y < Height; ++y)
				Parallel.For(0, Height, (y) =>
				//for (int y = 0; y < Height; y++)
				{
					byte* p = (byte*)(void*)Scan0 + y * stride;
					for (int x = 0; x < Width; ++x)
					{
						//float4 lookup = GetPixel4(x, y);
						float p0 = m_color[x + y * m_width];
						//lookup = lookup.Clamp(0.0f, 255.0f);
						float p1 = System.Math.Min(255, System.Math.Max(0, -p0));
						p0 = System.Math.Min(255, System.Math.Max(0, p0));
						byte bp0 = (byte)p0;
						p[2] = (byte)p1;
						p[1] = bp0;
						p[0] = 0;
						p += 3;
						//if (UseAlpha)
						//{
						//    p[0] = (byte)lookup.w;
						//    p++;
						//}

					}
					p += nOffset;
				});
			}
			finalColorBitmap.UnlockBits(bmData);

			return finalColorBitmap;
		}

		public void Draw(Graphics g, int x, int y)
		{
			Bitmap bmpTemp = DrawToBitmap();
			g.DrawImageUnscaled(bmpTemp, x, y);
			bmpTemp.Dispose();
		}

		public void GaussianKernel(float xPos, float yPos, float radius, float magnitude)
		{

			float2 center = new float2(xPos, yPos);
			float standardDeviation = 1.0f;
			int iRadius = (int)System.Math.Ceiling(radius);
			for (int y = -iRadius; y <= iRadius; y++)
			{
				for (int x = -iRadius; x <= iRadius; x++)
				{
					float2 uv = new float2((float)x, (float)y);
					float r = (uv.Length() * 10.0f) / radius;     // arbitrary scalar I made up to make the blur the right size
					float main = (1.0f / (float)System.Math.Sqrt(2.0f * (float)System.Math.PI * standardDeviation * standardDeviation)) * (float)System.Math.E;
					float exp = -(r * r) / (2.0f * standardDeviation * standardDeviation);
					float intensity = (float)System.Math.Pow(main, exp);
					float old = GetPixelSafe(x + (int)xPos, y + (int)yPos);
					SetPixelSafe(x + (int)xPos, y + (int)yPos, intensity * magnitude + old);
				}
			}
		}

		public void FileLoad(string filename)
		{
			System.Drawing.Image image = null;
			try
			{
				image = System.Drawing.Image.FromFile(filename);
			}
			catch
			{
				throw new Exception("Error loading file");
				//Console.WriteLine("Error loading");
				//return;
			}
			System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(image);
			ResetSize(image.Width, image.Height);
			//for (int y = 0; y < target.Height; y++)
			//    for (int x = 0; x < target.Width; x++)
			//    {
			//        target.SetPixel(x, y, new float3(
			//            bmp.GetPixel(x, y).R / 255.0f, bmp.GetPixel(x, y).G / 255.0f, bmp.GetPixel(x, y).B / 255.0f));
			//    }

			// GDI+ still lies to us - the return format is BGR, NOT RGB.
			System.Drawing.Imaging.BitmapData bmData = bmp.LockBits(
				new System.Drawing.Rectangle(0, 0, Width, Height),
				System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);//.Format24bppRgb);
			int stride = bmData.Stride;
			System.IntPtr Scan0 = bmData.Scan0;
			unsafe
			{
				byte* p = (byte*)(void*)Scan0;
				int nOffset = stride - Width * 4;
				//Marshal.Copy(Scan0, data, 0, Width * Height);
				for (int y = 0; y < Height; ++y)
				{
					for (int x = 0; x < Width; ++x)
					{
						//float4 pix = new float4();
						//pix.x = p[2];
						//pix.y = p[1];
						//pix.z = p[0];
						//pix.w = p[3];
						//SetPixel(x, y, pix);
						SetPixel(x, y, p[0]);	// Red channel only
						p += 4;
					}
					p += nOffset;
				}
			}
			bmp.UnlockBits(bmData);
			bmp.Dispose();
			image.Dispose();
		}

		public void FileSave(string filename)
		{
			System.Drawing.Bitmap bmp = DrawToBitmap();
			try
			{
				if (filename.ToLower().EndsWith("tif")) bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Tiff);	// tiff saves alpha channel.
				else if (filename.ToLower().EndsWith("bmp")) bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Bmp);
				else if (filename.ToLower().EndsWith("png")) bmp.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
				else throw new Exception("Invalid filetype for filesave: " + filename);
			}
			catch
			{
				Console.WriteLine("Error saving.");
			}
		}

		public void FileSaveRawUInt16(string filename)
		{
			UInt16[] u16 = new ushort[Width * Height];
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					u16[x + y * Width] = (UInt16)GetPixel(x, y);
				}
			}
			byte[] b = new byte[Width * Height * 2];
			Buffer.BlockCopy(u16, 0, b, 0, b.Length);
			File.WriteAllBytes(filename, b);
		}

		public void FourierLine(int yIndex, out BitmapFloat1 sinComp, out BitmapFloat1 cosComp)
		{
			sinComp = new BitmapFloat1(Width, 1);
			cosComp = new BitmapFloat1(Width, 1);
			for (int freq = 0; freq < Width; freq++)
			{
				float total = 0;
				float totalCos = 0;
				float totalSin = 0;
				for (int x = 0; x < Width; x++)
				{
					float val = GetPixel(x, yIndex);
					float angle = -((float)System.Math.PI * 2) * x * freq / ((float)Width);
					float sin = (float)System.Math.Sin(angle);
					float cos = (float)System.Math.Cos(angle);
					// if (!alpha)
					{
						total++;
						totalCos += val * cos;
						totalSin += val * sin;
					}
				}
				if (total > 0)
				{
					//totalSin /= total;
					//totalCos /= total;
					//float accum = (Math.Abs(totalCos) + Math.Abs(totalSin));
					sinComp.SetPixel(freq, 0, totalSin);
					cosComp.SetPixel(freq, 0, totalCos);
				}
				else throw new Exception("sdgsdg");
			}
		}

		public void FourierSkip(out BitmapFloat1 sinComp, out BitmapFloat1 cosComp, int offset, int skip)
		{
			sinComp = new BitmapFloat1(Width, 1);
			cosComp = new BitmapFloat1(Width, 1);
			for (int freq = 0; freq < Width / 2; freq++)
			{
				float totalCos = 0;
				float totalSin = 0;
				for (int x = offset; x < Width; x += skip)
				{
					float val = GetPixel(x, 0);
					float angle = -((float)System.Math.PI * 2) * x * freq / ((float)Width);
					float sin = (float)System.Math.Sin(angle);
					float cos = (float)System.Math.Cos(angle);
					totalCos += val * cos;
					totalSin += val * sin;
				}
				sinComp.SetPixel(freq, 0, totalSin);
				cosComp.SetPixel(freq, 0, totalCos);
			}
			for (int freq = Width / 2; freq < Width; freq++)
			{
				sinComp.SetPixel(freq, 0, -sinComp.GetPixel(freq - Width / 2, 0));
				cosComp.SetPixel(freq, 0, -cosComp.GetPixel(freq - Width / 2, 0));
			}
		}

		public static void FourierSkipComplex(BitmapFloat1 sinIn, BitmapFloat1 cosIn, out BitmapFloat1 sinComp, out BitmapFloat1 cosComp, int offset, int skip)
		{
			int w = sinIn.Width;
			sinComp = new BitmapFloat1(w, 1);
			cosComp = new BitmapFloat1(w, 1);
			for (int freq = 0; freq < w / 2; freq++)
			{
				float totalCos = 0;
				float totalSin = 0;
				for (int x = offset; x < w; x += skip)
				{
					float valSin = sinIn.GetPixel(x, 0);
					float valCos = cosIn.GetPixel(x, 0);
					float angle = -((float)System.Math.PI * 2) * x * freq / ((float)w);
					float sin = (float)System.Math.Sin(angle);
					float cos = (float)System.Math.Cos(angle);
					// (a+bi)(c+di) = (ac-bd) + (bc+ad)i
					// (valCos + valSin i)(cos + 0i) = (valCos*cos - 0) + (valSin*cos + 0)i
					// (valCos + valSin i)(0 + sin i) = (0 - valSin*sin) + (0 + valCos * sin)i
					totalCos += valCos * cos;
					totalSin += valSin * cos;
					totalCos += valSin * sin;
					totalSin += valCos * sin;
					//totalCos += val * cos;
					//totalSin += val * sin;
				}
				sinComp.SetPixel(freq, 0, totalSin);
				cosComp.SetPixel(freq, 0, totalCos);
			}
			for (int freq = w / 2; freq < w; freq++)
			{
				sinComp.SetPixel(freq, 0, -sinComp.GetPixel(freq - w / 2, 0));
				cosComp.SetPixel(freq, 0, -cosComp.GetPixel(freq - w / 2, 0));
			}
		}

		public static BitmapFloat1 InverseFourierLine(BitmapFloat1 sinComp, BitmapFloat1 cosComp, int bigWidth)
		{
			BitmapFloat1 result = new BitmapFloat1(bigWidth, 1);
			for (int freq = 0; freq < bigWidth; freq++)
			{
				//float4 pix = GetPixel4(freq, 0);
				float sinPart = sinComp.GetPixel(freq, 0);
				float cosPart = cosComp.GetPixel(freq, 0);
				for (int x = 0; x < bigWidth; x++)
				{
					float angle = ((float)System.Math.PI * 2) * x * freq / ((float)bigWidth);
					float sin = (float)System.Math.Sin(angle);
					float cos = (float)System.Math.Cos(angle);
					float final = sin * sinPart + cos * cosPart;
					result.AddPixel(x, 0, final);
				}
			}
			return result;
		}

		public void FourierBitmap(out BitmapFloat1 sinComp, out BitmapFloat1 cosComp)
		{
			BitmapFloat1 sinComp0 = new BitmapFloat1(Width, Height);
			BitmapFloat1 cosComp0 = new BitmapFloat1(Width, Height);
			for (int yIndex = 0; yIndex < Height; yIndex++)
			{
				for (int freq = 0; freq < Width; freq++)
				{
					float totalCos = 0;
					float totalSin = 0;
					for (int x = 0; x < Width; x++)
					{
						float val = GetPixel(x, yIndex);
						float angle = -((float)System.Math.PI * 2) * x * freq / ((float)Width);
						float sin = (float)System.Math.Sin(angle);
						float cos = (float)System.Math.Cos(angle);
						totalCos += val * cos;
						totalSin += val * sin;
					}
					//totalSin /= Width;
					//totalCos /= Width;
					sinComp0.SetPixel(freq, yIndex, totalSin);
					cosComp0.SetPixel(freq, yIndex, totalCos);
				}
			}
			sinComp = new BitmapFloat1(Width, Height);
			cosComp = new BitmapFloat1(Width, Height);
			for (int xIndex = 0; xIndex < Width; xIndex++)
			{
				for (int freq = 0; freq < Height; freq++)
				{
					float totalCos = 0;
					float totalSin = 0;
					for (int y = 0; y < Height; y++)
					{
						float valSin = sinComp0.GetPixel(xIndex, y);
						float valCos = cosComp0.GetPixel(xIndex, y);
						float angle = -((float)System.Math.PI * 2) * y * freq / ((float)Height);
						float sin = (float)System.Math.Sin(angle);
						float cos = (float)System.Math.Cos(angle);
						// (a+bi)(c+di) = (ac-bd) + (bc+ad)i
						// (valCos + valSin i)(cos + 0i) = (valCos*cos - 0) + (valSin*cos + 0)i
						// (valCos + valSin i)(0 + sin i) = (0 - valSin*sin) + (0 + valCos * sin)i
						totalCos += valCos * cos;
						totalSin += valSin * cos;
						totalCos += valSin * sin;
						totalSin += valCos * sin;
						//totalCos += val * cos;
						//totalSin += val * sin;
					}
					//totalSin /= Height;
					//totalCos /= Height;
					if (freq == 0)
					{
						sinComp.SetPixel(xIndex, freq, totalSin);
						cosComp.SetPixel(xIndex, freq, totalCos);
					}
					else
					{
						sinComp.SetPixel(xIndex, freq, totalSin);
						cosComp.SetPixel(xIndex, Height - freq, totalCos);
					}
				}
			}
		}

		public static BitmapFloat1 InverseFourierBitmap(BitmapFloat1 sinComp, BitmapFloat1 cosComp, float alpha=0)
		{
			int w = sinComp.Width;
			int h = sinComp.Height;
			BitmapFloat1 sinCompResult0 = new BitmapFloat1(w, h);
			BitmapFloat1 cosCompResult0 = new BitmapFloat1(w, h);
			for (int xIndex = 0; xIndex < w; xIndex++)
			{
				for (int freq = 0; freq < h; freq++)
				{
					float totalCos = 0;
					float totalSin = 0;
					for (int y = 0; y < h; y++)
					{
						float valSin = sinComp.GetPixel(xIndex, y);
						float valCos = cosComp.GetPixel(xIndex, y);
						if (y != 0)
						{
							valCos = cosComp.GetPixel(xIndex, h - y);
						}
						valCos = valCos * (1.0f - alpha) + valSin * alpha;
						float angle = -((float)System.Math.PI * 2) * y * freq / ((float)h);
						float sin = (float)System.Math.Sin(angle);
						float cos = (float)System.Math.Cos(angle);
						// (a+bi)(c+di) = (ac-bd) + (bc+ad)i
						// (valCos + valSin i)(cos + 0i) = (valCos*cos - 0) + (valSin*cos + 0)i
						// (valCos + valSin i)(0 + sin i) = (0 - valSin*sin) + (0 + valCos * sin)i
						totalCos += valCos * cos;
						totalSin += valSin * cos;
						totalCos += valSin * sin;
						totalSin += valCos * sin;
					}
					totalSin /= h;
					totalCos /= h;
					sinCompResult0.SetPixel(xIndex, freq, totalSin);
					cosCompResult0.SetPixel(xIndex, freq, totalCos);
				}
			}

			//BitmapFloat1 sinCompResult1 = new BitmapFloat1(w, h);
			BitmapFloat1 cosCompResult1 = new BitmapFloat1(w, h);
			for (int yIndex = 0; yIndex < h; yIndex++)
			{
				for (int freq = 0; freq < w; freq++)
				{
					float totalCos = 0;
					//float totalSin = 0;
					for (int x = 0; x < w; x++)
					{
						float valSin = sinCompResult0.GetPixel(x, yIndex);
						float valCos = cosCompResult0.GetPixel(x, yIndex);
						//float valSin = sinComp.GetPixel(x, yIndex);
						//float valCos = cosComp.GetPixel(x, yIndex);
						valCos = valCos * (1.0f - alpha) + valSin * alpha;
						float angle = -((float)System.Math.PI * 2) * x * freq / ((float)w);
						float sin = (float)System.Math.Sin(angle);
						float cos = (float)System.Math.Cos(angle);
						totalCos += valCos * cos;
						//totalSin += valSin * cos;
						totalCos += valSin * sin;
						//totalSin += valCos * sin;
					}
					//totalSin /= w;
					totalCos /= w;
					//sinCompResult1.SetPixel(freq, yIndex, totalSin);
					cosCompResult1.SetPixel(freq, yIndex, totalCos);
				}
			}

			return cosCompResult1;
		}

		public void DCT1D(out BitmapFloat1 cosComp)
		{
			cosComp = new BitmapFloat1(Width, Height);
			for (int yIndex = 0; yIndex < Height; yIndex++)
			{
				for (int freq = 0; freq < Width; freq++)
				{
					float totalCos = 0;
					for (int x = 0; x < Width; x++)
					{
						float val = GetPixel(x, yIndex);
						float angle = (float)((System.Math.PI / ((float)Width)) * (x + 0.5f) * freq);
						//double angle = (Math.PI * (2 * x + 1) * freq) / (2.0 * Width);
						float cos = (float)System.Math.Cos(angle);
						totalCos += val * cos;
					}
					if (freq != 0) totalCos *= (float)System.Math.Sqrt(2.0f / Width);
					else totalCos *= (float)System.Math.Sqrt(1.0f / Width);
					cosComp.SetPixel(freq, yIndex, totalCos);
				}
			}
		}

		// http://www.dcd.zju.edu.cn/~jun/Courses/Multimedia2011.../complementary/DCT_Theory%20and%20Application.pdf
		public static BitmapFloat1 InverseDCT1D(BitmapFloat1 cosComp)
		{
			BitmapFloat1 result = new BitmapFloat1(cosComp.Width, cosComp.Height);
			for (int yIndex = 0; yIndex < cosComp.Height; yIndex++)
			{
				for (int x = 0; x < cosComp.Width; x++)
				{
					for (int freq = 0; freq < cosComp.Width; freq++)
					{
						float cosPart = cosComp.GetPixel(freq, yIndex);
						float angle = (float)((System.Math.PI / ((float)cosComp.Width)) * (x + 0.5f) * freq);
						//double angle = (Math.PI * (2 * x + 1) * freq) / (2.0 * cosComp.Width);
						float cos = (float)System.Math.Cos(angle);
						float final = cos * cosPart;
						if (freq != 0) final *= (float)System.Math.Sqrt(2.0f / cosComp.Width);
						else final *= (float)System.Math.Sqrt(1.0f / cosComp.Width);
						result.AddPixel(x, yIndex, final);
					}
				}
			}
			return result;
		}

		// tested, works.
		public void DCT2D(out BitmapFloat1 cosComp)
		{
			BitmapFloat1 tempComp = new BitmapFloat1(Width, Height);
			//for (int freqY = 0; freqY < Height; freqY++)
			Parallel.For(0, Height, (freqY) =>
			{
				for (int freqX = 0; freqX < Width; freqX++)
				{
					double totalCos = 0;
					for (int y = 0; y < Height; y++)
					{
						for (int x = 0; x < Width; x++)
						{
							double val = GetPixel(x, y);
							double angleX = (double)((System.Math.PI / ((double)Width)) * (x + 0.5f) * freqX);
							double angleY = (double)((System.Math.PI / ((double)Width)) * (y + 0.5f) * freqY);
							//double angle = (Math.PI * (2 * x + 1) * freq) / (2.0 * Width);
							double cosX = (double)System.Math.Cos(angleX);
							double cosY = (double)System.Math.Cos(angleY);
							totalCos += val * cosX * cosY;
						}
					}
					if (freqX != 0) totalCos *= (double)System.Math.Sqrt(2.0f / Width);
					else totalCos *= (double)System.Math.Sqrt(1.0f / Width);
					if (freqY != 0) totalCos *= (double)System.Math.Sqrt(2.0f / Height);
					else totalCos *= (double)System.Math.Sqrt(1.0f / Height);
					tempComp.SetPixel(freqX, freqY, (float)totalCos);
				}
			});
			cosComp = tempComp;
		}

		// tested, works.
		public static BitmapFloat1 InverseDCT2D(BitmapFloat1 cosComp)
		{
			BitmapFloat1 result = new BitmapFloat1(cosComp.Width, cosComp.Height);
			for (int y = 0; y < cosComp.Height; y++)
			{
				for (int x = 0; x < cosComp.Width; x++)
				{
					double totalCos = 0;
					for (int freqX = 0; freqX < cosComp.Width; freqX++)
					{
						for (int freqY = 0; freqY < cosComp.Height; freqY++)
						{
							double cosPart = cosComp.GetPixel(freqX, freqY);
							double angleX = (double)((System.Math.PI / ((double)cosComp.Width)) * (x + 0.5f) * freqX);
							double angleY = (double)((System.Math.PI / ((double)cosComp.Height)) * (y + 0.5f) * freqY);
							//double angle = (Math.PI * (2 * x + 1) * freq) / (2.0 * cosComp.Width);
							double cosX = (double)System.Math.Cos(angleX);
							double cosY = (double)System.Math.Cos(angleY);
							double final = cosX * cosY * cosPart;
							if (freqX != 0) final *= (double)System.Math.Sqrt(2.0f / cosComp.Width);
							else final *= (double)System.Math.Sqrt(1.0f / cosComp.Width);
							if (freqY != 0) final *= (double)System.Math.Sqrt(2.0f / cosComp.Height);
							else final *= (double)System.Math.Sqrt(1.0f / cosComp.Height);
							totalCos += final;
						}
					}
					result.AddPixel(x, y, (float)totalCos);
				}
			}
			return result;
		}


		// tested, works.
		public void DCT2DSeperable(out BitmapFloat1 cosComp)
		{
			BitmapFloat1 tempComp = new BitmapFloat1(Width, Height);
			for (int yIndex = 0; yIndex < Height; yIndex++)
			{
				for (int freq = 0; freq < Width; freq++)
				{
					float totalCos = 0;
					for (int x = 0; x < Width; x++)
					{
						float val = GetPixel(x, yIndex);
						float angle = (float)((System.Math.PI / ((float)Width)) * (x + 0.5f) * freq);
						//double angle = (Math.PI * (2 * x + 1) * freq) / (2.0 * Width);
						float cos = (float)System.Math.Cos(angle);
						totalCos += val * cos;
					}
					if (freq != 0) totalCos *= (float)System.Math.Sqrt(2.0f / Width);
					else totalCos *= (float)System.Math.Sqrt(1.0f / Width);
					tempComp.SetPixel(freq, yIndex, totalCos);
				}
			}
			cosComp = new BitmapFloat1(Width, Height);
			for (int xIndex = 0; xIndex < Width; xIndex++)
			{
				for (int freq = 0; freq < Height; freq++)
				{
					float totalCos = 0;
					for (int y = 0; y < Height; y++)
					{
						float val = tempComp.GetPixel(xIndex, y);
						float angle = (float)((System.Math.PI / ((float)Height)) * (y + 0.5f) * freq);
						//double angle = (Math.PI * (2 * y + 1) * freq) / (2.0 * Width);
						float cos = (float)System.Math.Cos(angle);
						totalCos += val * cos;
					}
					if (freq != 0) totalCos *= (float)System.Math.Sqrt(2.0f / Height);
					else totalCos *= (float)System.Math.Sqrt(1.0f / Height);
					cosComp.SetPixel(xIndex, freq, totalCos);
				}
			}
		}

		// http://www.dcd.zju.edu.cn/~jun/Courses/Multimedia2011.../complementary/DCT_Theory%20and%20Application.pdf
		public static BitmapFloat1 InverseDCT2DSeparable(BitmapFloat1 cosComp)
		{
			BitmapFloat1 result = new BitmapFloat1(cosComp.Width, cosComp.Height);
			for (int yIndex = 0; yIndex < cosComp.Height; yIndex++)
			{
				for (int x = 0; x < cosComp.Width; x++)
				{
					for (int freq = 0; freq < cosComp.Width; freq++)
					{
						float cosPart = cosComp.GetPixel(freq, yIndex);
						float angle = (float)((System.Math.PI / ((float)cosComp.Width)) * (x + 0.5f) * freq);
						//double angle = (Math.PI * (2 * x + 1) * freq) / (2.0 * cosComp.Width);
						float cos = (float)System.Math.Cos(angle);
						float final = cos * cosPart;
						if (freq != 0) final *= (float)System.Math.Sqrt(2.0f / cosComp.Width);
						else final *= (float)System.Math.Sqrt(1.0f / cosComp.Width);
						result.AddPixel(x, yIndex, final);
					}
				}
			}
			return result;
		}

		public static BitmapFloat1 InverseDCT2DLame(BitmapFloat1 cosComp)
		{
			BitmapFloat1 result = new BitmapFloat1(cosComp.Width, cosComp.Height);
			for (int yIndex = 0; yIndex < cosComp.Height; yIndex++)
			{
				for (int freq = 0; freq < cosComp.Width; freq++)
				{
					//float4 pix = GetPixel4(freq, 0);
					float cosPart = cosComp.GetPixel(freq, yIndex);
					for (int x = 0; x < cosComp.Width; x++)
					{
						//float angle = (float)((Math.PI / ((float)cosComp.Width)) * (x + 0.5f) * freq);
						double angle = (System.Math.PI * (2 * x + 1) * freq) / (2.0 * cosComp.Width);
						float cos = (float)System.Math.Cos(angle);
						float final = cos * cosPart;
						//if (freq == 0) final *= 0.5f;
						result.AddPixel(x, yIndex, final);
					}
				}
			}
			return result;
		}

		public BitmapFloat1 Mul(float scale)
		{
			BitmapFloat1 target = new BitmapFloat1(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float p0 = GetPixel(x, y) * scale;
					target.SetPixel(x, y, p0);
				}
			}
			return target;
		}

		public BitmapFloat1 Sub(BitmapFloat1 other)
		{
			if (other.Width != Width) throw new Exception("different size bitmaps are bad");
			if (other.Height != Height) throw new Exception("different size bitmaps are bad");
			BitmapFloat1 target = new BitmapFloat1(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float p0 = GetPixel(x, y) - other.GetPixel(x, y);
					target.SetPixel(x, y, p0);
				}
			}
			return target;
		}

		public BitmapFloat1 Mul(BitmapFloat1 other)
		{
			int tempWidth = System.Math.Max(Width, other.Width);
			int tempHeight = System.Math.Max(Height, other.Height);
			BitmapFloat1 target = new BitmapFloat1(tempWidth, tempHeight);
			for (int y = 0; y < tempHeight; y++)
			{
				for (int x = 0; x < tempWidth; x++)
				{
					throw new Exception("this is a sub not a mul");
					float p0 = GetPixelZeroEdge(x+32, y+32) - other.GetPixelZeroEdge(x, y);
					target.SetPixel(x, y, p0);
				}
			}
			return target;
		}

		public BitmapFloat1 ScaleUpInteger(int scale)
		{
			BitmapFloat1 target = new BitmapFloat1(Width * scale, Height * scale);
			for (int y = 0; y < target.Height; y++)
			{
				for (int x = 0; x < target.Width; x++)
				{
					float pix = GetPixel(x / scale, y / scale);
					target.SetPixel(x, y, pix);
				}
			}
			return target;
		}

		public static void MulComplex(BitmapFloat1 a, BitmapFloat1 b, BitmapFloat1 c, BitmapFloat1 d, out BitmapFloat1 targetA, out BitmapFloat1 targetB)
		{
			// (a+bi)(c+di) = (ac-bd) + (bc+ad)i
			int tempWidth = System.Math.Max(a.Width, c.Width);
			int tempHeight = System.Math.Max(a.Height, c.Height);
			targetA = new BitmapFloat1(tempWidth, tempHeight);
			targetB = new BitmapFloat1(tempWidth, tempHeight);
			for (int y = 0; y < tempHeight; y++)
			{
				for (int x = 0; x < tempWidth; x++)
				{
					float pa = a.GetPixelZeroEdge(x + 00, y + 00);
					float pb = b.GetPixelZeroEdge(x + 00, y + 00);
					float pc = c.GetPixelZeroEdge(x, y);
					float pd = d.GetPixelZeroEdge(x, y);

					float resA = pa * pc - pb * pd;
					float resB = pb * pc + pa * pd;
					//float len = 1;// tempHeight;// (float)Math.Sqrt(resA * resA + resB * resB);
					float len = (float)System.Math.Sqrt(resA * resA + resB * resB);

					targetA.SetPixel(x, y, resA / len);
					targetB.SetPixel(x, y, resB / len);
				}
			}
		}

		public static void AngleDifference(BitmapFloat1 a, BitmapFloat1 b, BitmapFloat1 c, BitmapFloat1 d, out BitmapFloat1 targetA, out BitmapFloat1 targetB)
		{
			// (a+bi)(c+di) = (ac-bd) + (bc+ad)i
			int tempWidth = System.Math.Max(a.Width, c.Width);
			int tempHeight = System.Math.Max(a.Height, c.Height);
			targetA = new BitmapFloat1(tempWidth, tempHeight);
			targetB = new BitmapFloat1(tempWidth, tempHeight);
			for (int y = 0; y < tempHeight; y++)
			{
				for (int x = 0; x < tempWidth; x++)
				{
					float pa = a.GetPixelZeroEdge(x + 00, y + 00);
					float pb = b.GetPixelZeroEdge(x + 00, y + 00);
					float pc = c.GetPixelZeroEdge(x, y);
					float pd = d.GetPixelZeroEdge(x, y);

					float angle1 = (float)System.Math.Atan2(pb, pa);
					float angle2 = (float)System.Math.Atan2(pd, pc);
					float diff = angle1 - angle2;
					float resA = (float)System.Math.Cos(diff);
					float resB = (float)System.Math.Sin(diff);

					//					float resA = pa * pc - pb * pd;
					//				float resB = pb * pc + pa * pd;
					float len = 1;// tempHeight;// (float)Math.Sqrt(resA * resA + resB * resB);
					//float len = (float)Math.Sqrt(resA * resA + resB * resB);

					targetA.SetPixel(x, y, resA / len);
					targetB.SetPixel(x, y, resB / len);
				}
			}
		}

		public static void Crazy(ref BitmapFloat1 a, ref BitmapFloat1 b)
		{
			// (a+bi)(c+di) = (ac-bd) + (bc+ad)i
			int tempWidth = a.Width;
			int tempHeight = a.Height;
			for (int y = 0; y < tempHeight; y++)
			{
				for (int x = 0; x < tempWidth; x++)
				{
					float pa = a.GetPixelZeroEdge(x, y);
					float pb = b.GetPixelZeroEdge(x, y);

					//float angle1 = (float)Math.Atan2(pb, pa);
					//float angle2 = (float)Math.Atan2(pd, pc);
					//float diff = angle1 - angle2;
					//float resA = (float)Math.Cos(diff);
					//float resB = (float)Math.Sin(diff);

					////					float resA = pa * pc - pb * pd;
					////				float resB = pb * pc + pa * pd;
					//float len = 1;// tempHeight;// (float)Math.Sqrt(resA * resA + resB * resB);
					float len = (float)System.Math.Sqrt(pa * pa + pb * pb);

					a.SetPixel(x, y, pa / len);
					b.SetPixel(x, y, pb / len);
				}
			}
		}

		public BitmapFloat1 DownSample2x2Integer(bool round = false)
		{
			BitmapFloat1 down = new BitmapFloat1(Width / 2, Height / 2);
			for (int y = 0; y < Height-1; y++)
			{
				for (int x = 0; x < Width-1; x++)
				{
					float pix = GetPixel(x, y);
					pix += GetPixel(x + 1, y);
					pix += GetPixel(x, y + 1);
					pix += GetPixel(x + 1, y + 1);
					//pix = (float)Math.Round(pix / 4.0f);
					pix = pix / 4.0f;
					down.SetPixel(x / 2, y / 2, round?(int)pix:pix);
				}
			}
			return down;
		}

		public BitmapFloat1 DiffFromDownSample(BitmapFloat1 small)
		{
			BitmapFloat1 diff = new BitmapFloat1(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float smallPix = small.GetPixel(x / 2, y / 2);
					float pix = GetPixel(x, y);
					pix -= smallPix;
					diff.SetPixel(x, y, pix);
				}
			}
			return diff;
		}

		public BitmapFloat1 LastPixelDeltaUpperLeft()
		{
			BitmapFloat1 delta = new BitmapFloat1(Width, Height);
			float lastPixel = 0;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float pixU = GetPixelZeroEdge(x, y - 1);
					float pixUL = GetPixelZeroEdge(x - 1, y - 1);
					float upDelta = pixU - pixUL;
					float pix = GetPixel(x, y);
					float pixDelta = pix - lastPixel - upDelta;
					lastPixel = pix;
					delta.SetPixel(x, y, pixDelta);
				}
			}
			return delta;
		}

		public BitmapFloat1 LastPixelDeltaHorizontal()
		{
			BitmapFloat1 delta = new BitmapFloat1(Width, Height);
			float lastPixel = 0;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float pix = GetPixel(x, y);
					float pixDelta = pix - lastPixel;
					lastPixel = pix;
					delta.SetPixel(x, y, pixDelta);
				}
			}
			return delta;
		}

		public BitmapFloat1 LastPixelDeltaVertical()
		{
			BitmapFloat1 delta = new BitmapFloat1(Width, Height);
			float lastPixel = 0;
			for (int x = 0; x < Width; x++)
			{
				for (int y = 0; y < Height; y++)
				{
					float pix = GetPixel(x, y);
					float pixDelta = pix - lastPixel;
					lastPixel = pix;
					delta.SetPixel(x, y, pixDelta);
				}
			}
			return delta;
		}

		public BitmapFloat1 LastPixelDeltaDiagonalSE()
		{
			BitmapFloat1 delta = new BitmapFloat1(Width, Height);
			float lastPixel = 0;
			for (int x = 0; x < Width; x++)
			{
				for (int y = 0; y < Height; y++)
				{
					float pix = GetPixel(x, y);
					lastPixel = GetPixelZeroEdge(x - 1, y - 1);
					float pixDelta = pix - lastPixel;
					lastPixel = pix;
					delta.SetPixel(x, y, pixDelta);
				}
			}
			return delta;
		}

		//public void Compress()
		//{
		//    byte[] stream = new byte[Width * Height];
		//    for (int y = 0; y < Height; y++)
		//    {
		//        for (int x = 0; x < Width; x++)
		//        {
		//            float pix = GetPixel(x, y);
		//            stream[x + y * Width] = (byte)pix;
		//        }
		//    }

		//    FileStream f2 = new FileStream("compessedTest.deleteme.bin", FileMode.Create);
		//    byte[] compressed = Ionic.Zlib.ZlibStream.CompressBuffer(stream);
		//    GZipStream Compress = new GZipStream(f2, CompressionMode.Compress);
		//    Compress.Write(stream, 0, stream.Length);
		//    // Copy the source file into 
		//    // the compression stream.
		//    //inFile.CopyTo(Compress);
		//    FileInfo fi = new FileInfo("compessedTest.deleteme.bin");
		//    Console.WriteLine("Compressed from {0} to {1} bytes. (dictionary)",
		//        stream.Length.ToString(), compressed.Length.ToString());
		//    Compress.Dispose();
		//    f2.Dispose();
		//}

		public void Histogram()
		{
			int small = 0;
			int big = 0;
			int[] hist = new int[1025];
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float pix = GetPixel(x, y);
					short temp = (short)pix;
					if (System.Math.Abs(temp) < 8) small++;
					else big++;
					hist[temp + 512]++;
				}
			}
			//for (int count = 500; count < 530; count++) Console.WriteLine((count-512).ToString() + "   " + hist[count].ToString());
			//Console.WriteLine("small: " + small + "   big: " + big + "    " + ((float)small/big));
		}

		public void DrawHistogram(Graphics g)
		{
			g.FillRectangle(Brushes.Black, 256, 0, 512, 16);
			SortedDictionary<int, int> histogram = new SortedDictionary<int, int>();
			int max = 0;
			foreach (int b in m_color)
			{
				if (histogram.ContainsKey(b)) histogram[b]++;
				else histogram.Add(b, 1);
				max = System.Math.Max(histogram[b], max);
			}

			int range = histogram.Last().Key - histogram.First().Key;
			for (int count = -512; count < 512; count++)
			{
				int size;
				bool good = histogram.TryGetValue(count + histogram.First().Key, out size);
				size = (size * 256) / max;
				if (good)
				{
					g.DrawLine(Pens.LimeGreen, count+512, 0, count+512, size);
				}
			}
			for (int count = -512; count < 512; count++)
			{
				if ((count & 15) == 0) g.DrawLine(Pens.DarkGreen, count + 512, 0, count + 512, 16);
			}
		}

		public float CornerKernelDiff(int xMe, int yMe, float x2, float y2, out float delta)
		{
			float total = 0;
			float pix1L = GetPixelBilinear(new float2(xMe - 1, yMe));
			float pix2L = GetPixelBilinear(new float2(x2 - 1, y2));
			float pix1UL = GetPixelBilinear(new float2(xMe - 1, yMe - 1));
			float pix2UL = GetPixelBilinear(new float2(x2 - 1, y2 - 1));
			float pix1U = GetPixelBilinear(new float2(xMe, yMe - 1));
			float pix2U = GetPixelBilinear(new float2(x2, y2 - 1));
			float pix1UR = GetPixelBilinear(new float2(xMe + 1, yMe - 1));
			float pix2UR = GetPixelBilinear(new float2(x2 + 1, y2 - 1));
			float avg1 = (pix1L + pix1U + pix1UL + pix1UR) / 4;
			float avg2 = (pix2L + pix2U + pix2UL + pix2UR) / 4;
			//avg1 = (int)(avg1);
			//avg2 = (int)(avg2);
			//float avg1 = pix1L;
			//float avg2 = pix2L;
			//pix1L -= avg1;
			//pix1UL -= avg1;
			//pix1U -= avg1;
			//pix1UR -= avg1;
			//pix2L -= avg2;
			//pix2UL -= avg2;
			//pix2U -= avg2;
			//pix2UR -= avg2;
			float pixGuess = GetPixelBilinear(new float2(x2, y2));
			delta = pixGuess - avg2 + avg1;
			total += (pix1L - pix2L) * (pix1L - pix2L) + (pix1UL - pix2UL) * (pix1UL - pix2UL) +
				(pix1U - pix2U) * (pix1U - pix2U) + (pix1UR - pix2UR) * (pix1UR - pix2UR);
			return total;
		}

		public BitmapFloat1 CornerKernelTransform(bool forward = true)
		{
			BitmapFloat1 delta = new BitmapFloat1(Width, Height);
			BitmapFloat1 source = this;
			if (!forward) source = delta;
			for (int y = 0; y < Height; y++)
			//Parallel.For(0, Height, (y) =>
			{
				for (int x = 0; x < Width; x++)
				{
					float pixDeltaL, pixDeltaU, pixDeltaUL, pixDeltaUR, pixDeltaULFN, pixDeltaURF;
					float left = source.CornerKernelDiff(x, y, x - 1, y, out pixDeltaL);
					float up = source.CornerKernelDiff(x, y, x, y - 1, out pixDeltaU);
					float upLeft = source.CornerKernelDiff(x, y, x - 1, y - 1, out pixDeltaUL);
					float upLeftFracN = source.CornerKernelDiff(x, y, x - 0.5f, y - 1, out pixDeltaULFN);
					float upRightFrac = source.CornerKernelDiff(x, y, x + 0.5f, y - 1, out pixDeltaURF);
					float upRight = source.CornerKernelDiff(x, y, x + 1, y - 1, out pixDeltaUR);
					float best = left;
					float pixDelta = pixDeltaL;
					if (up * 2 < best)
					{
						pixDelta = pixDeltaU;
						best = up;
					}
					//small: 13060   big: 3324    3.929001
					//Compressed from 16384 to 9713 bytes. (dictionary)
					//Compressed 16384 bytes to 9124
					if (upLeft * 9 < best)
					{
						pixDelta = pixDeltaUL;
						best = upLeft;
					}
					if (upLeftFracN * 3 < best)
					{
						pixDelta = pixDeltaULFN;
						best = upLeftFracN;
					}
					if (upRightFrac * 2 < best)
					{
						pixDelta = pixDeltaURF;
						best = upRightFrac;
					}
					if (upRight * 6 < best)
					{
						pixDelta = pixDeltaUR;
						best = upRight;
					}
					//if (best > 2000) pixDelta = GetPixelZeroEdge(x - 1, y);
					//pixDelta = (int)pixDelta;
					if (forward)
					{
						float pix = source.GetPixel(x, y);
						float final = (float)(pix - (int)(pixDelta - 0.0f));
						//if (Math.Abs(final) <= 2.0f) final = 0.0f;
						delta.SetPixel(x, y, final);
					}
					else
					{
						float pix = GetPixelZeroEdge(x, y);
						float final = (float)(pix + (int)(pixDelta - 0.0f));
						delta.SetPixel(x, y, final);
					}
				}
			}//);
			return delta;
		}

		//public BitStream HuffmanCode(int maxClamp = int.MaxValue)
		//{
		//    int size = Width * Height;
		//    size = Math.Min(maxClamp, size);
		//    int[] sourceData = new int[size];
		//    for (int count = 0; count < sourceData.Length; count++) sourceData[count] = (int)m_color[count];
		//    Huffman huff = new Huffman();
		//    huff.MakeTree(sourceData);
		//    BitStream encoded = huff.Encode(sourceData);
		//    huff.Decode(encoded);
		//    return encoded;
		//}

		//public BitStream ArithmeticCode(int maxClamp = int.MaxValue)
		//{
		//    int size = Width * Height;
		//    size = Math.Min(maxClamp, size);
		//    int[] sourceData = new int[size];
		//    for (int count = 0; count < sourceData.Length; count++) sourceData[count] = (int)m_color[count];
		//    Arithmetic arith = new Arithmetic();
		//    arith.GenRanges(sourceData);
		//    BitStream encoded = arith.Compress(sourceData);
		//    Console.WriteLine("Arith Compressed " + size + " bytes to " + (encoded.frontIndex/8));

		//    //huff.Decode(encoded);
		//    return encoded;
		//}

		public BitmapFloat1 PredictFromOtherChannel()
		{
			BitmapFloat1 target = new BitmapFloat1(Width, Height);
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float pix1L = GetPixelZeroEdge(x - 1, y);
					float pixTL = target.GetPixelZeroEdge(x - 1, y);
					float pix1UL = GetPixelZeroEdge(x - 1, y - 1);
					float pixTUL = target.GetPixelZeroEdge(x - 1, y - 1);
					float pix1U = GetPixelZeroEdge(x, y - 1);
					float pixTU = target.GetPixelZeroEdge(x, y - 1);
					float pix1UR = GetPixelZeroEdge(x + 1, y - 1);
					float pixTUR = target.GetPixelZeroEdge(x + 1, y - 1);
					float avg1 = (pix1L + pix1UL + pix1U + pix1UR) * 0.25f;
					float avgT = (pixTL + pixTUL + pixTU + pixTUR) * 0.25f;

					float pixGuess = GetPixel(x, y);
					float delta = pixGuess + avg1 - avgT;
					target.SetPixel(x, y, delta);
				}
			}

			return target;
		}

		public float DotProduct(BitmapFloat1 other)
		{
			float total = 0;
			for (int count = 0; count < m_color.Length; count++)
			{
				total += m_color[count] * other.m_color[count];
			}
			return total;
		}

		public float Diff(BitmapFloat1 other)
		{
			float total = 0;
			for (int count = 0; count < m_color.Length; count++)
			{
				float delta = m_color[count] - other.m_color[count];
				total += delta * delta;
			}
			return total;
		}

		public BitmapFloat1 Max(float a)
		{
			BitmapFloat1 target = new BitmapFloat1(Width, Height);
			for (int i = 0; i < m_color.Length; i++) target.m_color[i] = System.Math.Max(m_color[i], a);
			return target;
		}

		public BitmapFloat1 Normalize(float a = 1.0f)
		{
			BitmapFloat1 target = new BitmapFloat1(Width, Height);
			float maxPixel = -float.MaxValue;
			for (int i = 0; i < m_color.Length; i++) maxPixel = System.Math.Max(maxPixel, m_color[i]);
			for (int i = 0; i < m_color.Length; i++) target.m_color[i] = (m_color[i] * a) / maxPixel;
			return target;
		}

		public static BitmapFloat1 GenRandom(int w, int h, float min, float max, Random rand)
		{
			BitmapFloat1 target = new BitmapFloat1(w, h);
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					target.SetPixel(x, y, (float)(rand.NextDouble() * (max - min) + min));
				}
			}
			return target;
		}

		public void CopyToRegion(BitmapFloat1 source, int destX, int destY)
		{
			for (int y = 0; y < source.Height; y++)
			{
				for (int x = 0; x < source.Width; x++)
				{
					float pix00 = source.GetPixel(x, y);
					SetPixel(x + destX, y + destY, pix00);
				}
			}
		}

		public static BitmapFloat1 PackMultiple(List<BitmapFloat1> source)
		{
			int w2 = source.Count * source[0].Width;
			BitmapFloat1 result = new BitmapFloat1(w2, source[0].Height);
			for (int i = 0; i < source.Count; i++)
			{
				result.CopyToRegion(source[i], i * source[0].Width, 0);
			}
			return result;
		}

		public int2 FindMaxPixel()
		{
			int2 bestIndex = new int2();
			float max = -float.MaxValue;
			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					float pix00 = GetPixel(x, y);
					if (pix00 > max)
					{
						max = pix00;
						bestIndex = new int2(x, y);
					}
				}
			}
			return bestIndex;
		}

	}
}

﻿#region ApacheLicense2.0
// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Diagnostics;
using System.Threading;
using Otavio.Math;

using CudaInterface;

namespace Nautilus
{
	public partial class Form1 : Form
	{
		Random rand = new Random(0);
		Bitmap frameBuffer;
		CudaInterface.CudaImageF cuImage;
		CudaImageF waveHeight, waveVel, waveWalls, beenThere, corr0, corr1;
		int2 waveSpaceSize = new int2(768, 768);//1664, 1024);
		float2 particle;
		float2 particleVel;
		//CudaInterface.CudaImageF cuRand0, cuRand1;

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			frameBuffer = new Bitmap(ClientSize.Width, ClientSize.Height);
			//this.BackgroundImage = BackBuffer;
			//cuImage = new CudaInterface.CudaImageF("char128x128.png");
			//cuImage = new CudaInterface.CudaImageF(@"\dev\Globe\Screenshots\Alien2.png");
			//cuImage = new CudaInterface.CudaImageF(@"\dev\Globe\Content\Custom\cloud_dense_4096x4096.jpg");
			//cuImage.GenPerlin(32, 0);
			//waveWalls = new CudaImageF(@"DoubleSlit1664x1024.png");
			//waveWalls = new CudaImageF(@"Lens01.png");
			//waveWalls = new CudaInterface.CudaImageF(@"\dev\Globe\Content\Custom\cloud_dense_4096x4096.jpg");
			waveWalls = new CudaImageF(@"\dev\Globe\Screenshots\Alien2.png")*2;
			//waveWalls = new CudaImageF(@"Spiral.png");
			//waveWalls = new CudaImageF(@"Spiral512x512.png");
			//waveWalls.TryFourier();
			//waveWalls.SetPixel(10, 10, 100000);
			//waveWalls = new CudaImageF(waveSpaceSize.x, waveSpaceSize.y);
			//waveWalls.GenPerlin(32, 0);
			//waveWalls = waveWalls * 0.002f;
			//waveWalls.Clear();
			waveSpaceSize = new int2(waveWalls.GetWidth(), waveWalls.GetHeight());
			waveHeight = new CudaImageF(waveSpaceSize.x, waveSpaceSize.y);
			waveVel = new CudaImageF(waveSpaceSize.x, waveSpaceSize.y);
			beenThere = new CudaImageF(waveSpaceSize.x, waveSpaceSize.y);
			waveHeight.Clear();
			waveVel.Clear();
			beenThere.Clear();
			corr0 = new CudaImageF(@"GOPR0727lens.png");
			corr1 = new CudaImageF(@"GOPR0728lensBlur.png");
		}

		//protected override void OnPaint(PaintEventArgs e)
		//{
		//}

		//protected override void OnPaintBackground(PaintEventArgs e)
		//{
		//}

		static float frameCount = 0;
		DateTime dt = DateTime.Now;
		bool running = true;
		private void Form1_Paint(object sender, PaintEventArgs e)
		{
			Stopwatch stopWatch = new Stopwatch();
			stopWatch.Start();
			Graphics g = e.Graphics;
			frameCount += 1;

			//DoRadioStuff(g, (int)frameCount);

			//TerrainRenderJustForFun(g, (int)frameCount);

			//int[] bounds = new int[4];
			//cuImage.FindExtents(bounds);

			//CudaImageF small = new CudaImageF(64, 64);
			//small.CopyFromScaledRegion(cuImage, bounds);

			//(small).DrawToBitmap(frameBuffer);
			//g.DrawImageUnscaled(frameBuffer, 0, 0);
			CudaImageF destHeight = new CudaImageF(waveSpaceSize.x, waveSpaceSize.y);
			CudaImageF destVel = new CudaImageF(waveSpaceSize.x, waveSpaceSize.y);
			if (running)
			{
				for (int iter = 0; iter < 16; iter++)
				{
					destHeight.WaveGaussian(destVel, waveHeight, waveWalls, waveVel);
					CudaImageF temp1 = destHeight; destHeight = waveHeight; waveHeight = temp1;
					temp1 = destVel; destVel = waveVel; waveVel = temp1;
					//beenThere.SetPixelSafe((int)particle.x, (int)particle.y, 255);
					particle += particleVel;
					//if (particle != new float2(0, 0)) particleVel = FindDerivative(waveHeight, waveWalls, particle);
				}
			}
			//(waveHeight + waveWalls + beenThere).DrawToBitmap(frameBuffer);
			(waveHeight + waveWalls).DrawToBitmap(frameBuffer);
			/*			CudaImageF fourier = CudaImageF.FourierForward(waveWalls);
									fourier = CudaImageF.ComplexConjugate(fourier);
									CudaImageF inverse = CudaImageF.FourierInverse(fourier);
									CudaImageF bigger = new CudaImageF(fourier.GetWidth()*1, fourier.GetHeight()*1);
									CudaImageF mag = null;// = new CudaImageF(fourier.GetWidth() / 2, fourier.GetHeight() / 2);
									CudaImageF phase = null;// = new CudaImageF(fourier.GetWidth() / 2, fourier.GetHeight() / 2);
									fourier.ComplexSplitToPhaseMagnitude(ref phase, ref mag);
									bigger.Scale(fourier, 1);

									CudaImageF fcorr0 = CudaImageF.FourierForward(corr0);
									CudaImageF fcorr1 = CudaImageF.FourierForward(corr1);
									CudaImageF cc = CudaImageF.ComplexConjugate(fcorr0);
									CudaImageF cmul = CudaImageF.ComplexMultiply(cc, fcorr1);
									inverse = CudaImageF.FourierInverse(cmul);
									//inverse.ComplexBandPass(100, 200);
									//float debug = inverse.GetPixel(100, 100);
									(inverse * 0.04f).DrawToBitmap(frameBuffer);
									//CudaImageF cuScaled = new CudaImageF(1024, 768);
									//cuScaled.Clear();
									//cuScaled.TerrainRender(destHeight+2048, 0*frameCount);
									//(cuScaled).DrawToBitmap(frameBuffer);*/
			g.DrawImageUnscaled(frameBuffer, 0, 0);

			g.FillEllipse(Brushes.Blue, particle.x - 2, particle.y - 2, 5, 5);
			GC.Collect();

			stopWatch.Stop();
			//if ((DateTime.Now - dt).TotalMilliseconds < 16) Thread.Sleep(16 - (int)(DateTime.Now - dt).TotalMilliseconds);
			TimeSpan span = DateTime.Now - dt;
			//if (span < new TimeSpan(166666)) Thread.Sleep(new TimeSpan(166666) - span);
			//double stop = 1000 * (double)stopWatch.ElapsedTicks / Stopwatch.Frequency;
			//if (stop < 16.66666) Thread.Sleep(new TimeSpan((long)(166666)) - new TimeSpan((long)(stop * 10000)));

			this.Text = (span.TotalMilliseconds).ToString("F4") + "\t\t      " + stopWatch.ElapsedTicks.ToString();// +"      " + stop.ToString();
			dt = DateTime.Now;

			Invalidate();
		}

		public void DoRadioStuff(Graphics g, int frameCount)
		{
			string sourceString = "Hello World!";
			byte[] sourceBytes = ASCIIEncoding.ASCII.GetBytes(sourceString);
			CudaImageF sourceSignal = new CudaImageF(256, 1);
			sourceSignal.Clear();
			for (int i = 0; i < sourceString.Length; i++)
			{
				sourceSignal.SetPixel(i, 0, sourceBytes[i]);
			}

			CudaImageF fourier = CudaImageF.FourierForward(sourceSignal);
			CudaImageF ifft = CudaImageF.FourierInverse(fourier);

			(CudaImageF.Scale_(sourceSignal, 32)).Draw(g, 0, 0);//.DrawToBitmap(frameBuffer);
			(CudaImageF.Scale_(fourier, 32)).Draw(g, 0, 34);
			(CudaImageF.Scale_(ifft * 0.1f, 32)).Draw(g, 0, 68);
/*			float[] fa = new float[fourier.GetWidth() * fourier.GetHeight()];
			ifft.GetAsFloatArray(fa);
			//fa = fa.Select(a => a / 27.0f).ToArray();
			for (int i = 0; i < fa.Length; i++)
			{
				//fa[i] /= 27.0f;
			}
			int scale = 32;
			int scaleY = 1;
			for (int i = 0; i < fa.Length - 1; i++)
			{
				//g.DrawLine(Pens.Red, i * scale + scale / 2, fa[i] * scaleY + 256, (i + 1) * scale + scale / 2, fa[i + 1] * scaleY + 256);
			}*/
			//g.DrawImageUnscaled(frameBuffer, 0, 0);
			GC.Collect();

		}

		public float2 FindDerivative(CudaImageF source, CudaImageF wallSource, float2 pos)
		{
			float pix00 = source.GetPixelSafe((int)pos.x - 1, (int)pos.y - 1);
			float pix10 = source.GetPixelSafe((int)(pos.x + 1), (int)pos.y - 1);
			float pix01 = source.GetPixelSafe((int)pos.x - 1, (int)(pos.y+1));
			float pix11 = source.GetPixelSafe((int)(pos.x+1), (int)(pos.y+1));
			float delX = ((pix10 - pix00) + (pix11 - pix01)) * 0.5f;
			float delY = ((pix01 - pix00) + (pix11 - pix10)) * 0.5f;
			return -(new float2(delX, delY).Normalize())*1.0675f*0.5f;
			//return -new float2(delX, delY) * 0.002f;// *1.065f;
		}

		public void TerrainRenderJustForFun(Graphics g, int frameCount)
		{
			// 24000
			CudaImageF noise0 = new CudaImageF(2, 2);
			noise0.GenRandom(0);
			CudaImageF noise1 = new CudaImageF(4, 4);
			noise1.GenRandom(4096);
			CudaImageF noise2 = new CudaImageF(8, 8);
			noise2.GenRandom(8192);
			CudaImageF noise3 = new CudaImageF(16, 16);
			noise3.GenRandom(8192 + 4096);
			CudaImageF noise4 = new CudaImageF(32, 32);
			noise4.GenRandom(16384);
			CudaImageF noise5 = new CudaImageF(64, 64);
			noise5.GenRandom(16384 + 4096);
			CudaImageF noise6 = new CudaImageF(128, 128);
			noise6.GenRandom(16384 + 8192);
			CudaImageF combined = CudaImageF.ScaleBicubic_(noise0, 256) * 7 +
				CudaImageF.ScaleBicubic_(noise1, 128) * 6 +
				CudaImageF.ScaleBicubic_(noise2, 64) * 5 +
				CudaImageF.ScaleBicubic_(noise3, 32) * 2 +
				CudaImageF.ScaleBicubic_(noise4, 16) * 1 +
				CudaImageF.ScaleBicubic_(noise5, 8) * 0.3f +
				CudaImageF.ScaleBicubic_(noise6, 4) * 0.1f +
				-2048;
			//(combined / 4 - 340).DrawToBitmap(frameBuffer);

			//CudaImageF combined = new CudaImageF(512, 512);
			//CudaImageF combined2 = new CudaImageF(512, 512);
			//CudaImageF tempImage0 = new CudaImageF(512, 512);
			//CudaImageF tempImage1 = new CudaImageF(512, 512);
			//tempImage0.GenPerlin(8, 0);
			//tempImage1.GenPerlin(16, 128);
			//combined = tempImage0 + tempImage1;
			//tempImage0.GenPerlin(32, 256);
			//tempImage1.GenPerlin(64, 384);
			//combined2 = tempImage0 + tempImage1;
			//tempImage0 = combined + combined2;
			//tempImage1.GenPerlin(128, 512);
			//combined = tempImage0 + tempImage1;
			//(combined / 1024).DrawToBitmap(frameBuffer);

			//int[] bounds = new int[4];
			//combined.FindExtents(bounds);

			//cuImage = cuImage + 2;
			//CudaInterface.CudaImageF cuTrans = CudaImageF.Rotate_(cuImage, -frameCount * 0.0025f);
			CudaImageF cuScaled = new CudaImageF(1024, 768);
			cuScaled.Clear();
			cuScaled.TerrainRender(combined, frameCount);
			//cuScaled.GenRandom();
			//cuScaled.Sub(cuTrans, cuImage);
			//cuImage.GenRandom();
			//if (frameCount % 16 < 8) cuScaled.ScaleBicubic(cuTrans, 8.0f);
			//else cuScaled.Scale(cuTrans, 8.0f);
			(cuScaled).DrawToBitmap(frameBuffer);
			g.DrawImageUnscaled(frameBuffer, 0, 0);
			GC.Collect();
		}

		private void Form1_Resize(object sender, EventArgs e)
		{
			if (frameBuffer != null) frameBuffer.Dispose();
			if ((ClientSize.Height > 0) && (ClientSize.Width > 0)) frameBuffer = new Bitmap(ClientSize.Width, ClientSize.Height);
		}

		private void Form1_MouseUp(object sender, MouseEventArgs e)
		{
			waveHeight.Clear();
			waveVel.Clear();
			float size = 4;
			CudaImageF waveTemp = new CudaImageF(waveHeight.GetWidth(), waveHeight.GetHeight());
			waveTemp.GenerateGaussianKernel(e.X, e.Y, size, 21250);
			waveHeight = waveHeight + waveTemp;
			particle = new float2(e.X, e.Y);
			particleVel = new float2((float)rand.NextDouble() - 0.5f, (float)rand.NextDouble() - 0.5f).Normalize();
			particle += particleVel * size;
		}

		private void Form1_KeyPress(object sender, KeyPressEventArgs e)
		{
			running = !running;
		}
	}
}

// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "CudaInterfaceF.h"

#include "CudaFloat.h"

CudaInterface::CudaImageF::CudaImageF(int width, int height)
{
	wrapper = new CudaWrapperF(width, height);
}

CudaInterface::CudaImageF::!CudaImageF()
{
	delete wrapper;
}

CudaInterface::CudaImageF::CudaImageF(array<Byte> ^bufferAsBytes, int width, int height)
{
	wrapper = new CudaWrapperF(width, height);
	assert(wrapper->info.pitchInFloats == width);	// if not, implement strided copy.
	pin_ptr<Byte> p = &bufferAsBytes[0];
	wrapper->SetFromFloatArray((float*)p);
}

CudaInterface::CudaImageF::CudaImageF(String ^filename)
{
	Bitmap ^bmp = gcnew Bitmap(filename);
	int w = bmp->Width;
	int h = bmp->Height;
	Rectangle rect = Rectangle(0,0,w,h);
	System::Drawing::Imaging::BitmapData^ bmpData = bmp->LockBits( rect, System::Drawing::Imaging::ImageLockMode::ReadWrite, bmp->PixelFormat );
	int bytesPerPixel = 3;
	if (bmp->PixelFormat != System::Drawing::Imaging::PixelFormat::Format24bppRgb) bytesPerPixel = 4;

	// Get the address of the first line.
	IntPtr ptr = bmpData->Scan0;

	// Declare an array to hold the bytes of the bitmap.
	// This code is specific to a bitmap with 24 bits per pixels.
	int bytes = Math::Abs(bmpData->Stride) * h;
	array<Byte>^rgbValues = gcnew array<Byte>(bytes);

	// Copy the RGB values into the array.
	System::Runtime::InteropServices::Marshal::Copy( ptr, rgbValues, 0, bytes );

	wrapper = new CudaWrapperF(w, h);
	float *fValues = new float[wrapper->info.pitchInFloats * h];
	int stride = bmpData->Stride;
	int index = 0;
	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			//wrapper->SetPixel(x, y, rgbValues[(y * stride + x*bytesPerPixel) + 2]);
			fValues[y * wrapper->info.pitchInFloats + x] = rgbValues[(y * stride + x*bytesPerPixel) + 2];
			index++;
		}
	}
	wrapper->SetFromFloatArray(fValues);
	delete fValues;

	// Unlock the bits.
	bmp->UnlockBits( bmpData );
}

CudaInterface::CudaImageF::CudaImageF(Bitmap ^bmp)
{
	int w = bmp->Width;
	int h = bmp->Height;
	Rectangle rect = Rectangle(0,0,w,h);
	System::Drawing::Imaging::BitmapData^ bmpData = bmp->LockBits( rect, System::Drawing::Imaging::ImageLockMode::ReadWrite, bmp->PixelFormat );
	int bytesPerPixel = 3;
	if (bmp->PixelFormat != System::Drawing::Imaging::PixelFormat::Format24bppRgb) bytesPerPixel = 4;

	// Get the address of the first line.
	IntPtr ptr = bmpData->Scan0;

	// Declare an array to hold the bytes of the bitmap.
	// This code is specific to a bitmap with 24 bits per pixels.
	int bytes = Math::Abs(bmpData->Stride) * h;
	array<Byte>^rgbValues = gcnew array<Byte>(bytes);

	// Copy the RGB values into the array.
	System::Runtime::InteropServices::Marshal::Copy( ptr, rgbValues, 0, bytes );

	wrapper = new CudaWrapperF(w, h);
	float *fValues = new float[wrapper->info.pitchInFloats * h];
	int stride = bmpData->Stride;
	int index = 0;
	for (int y = 0; y < h; y++)
	{
		for (int x = 0; x < w; x++)
		{
			//wrapper->SetPixel(x, y, rgbValues[(y * stride + x*bytesPerPixel) + 2]);
			fValues[y * wrapper->info.pitchInFloats + x] = rgbValues[(y * stride + x*bytesPerPixel) + 2];
			index++;
		}
	}
	wrapper->SetFromFloatArray(fValues);
	delete fValues;

	// Unlock the bits.
	bmp->UnlockBits( bmpData );
}

static CudaInterface::CudaImageF::CudaImageF()
{
	randImage0 = new CudaWrapperF(113, 127);	// 113 * 127 = 14351 pixels till repeat, just inside the min pitch of 128.
	randImage1 = new CudaWrapperF(127, 113);
	GenInitialRandom(randImage0, 0);
	GenInitialRandom(randImage1, 1);
	randOffset = 0;
}

void CudaInterface::CudaImageF::DrawToBitmap(Bitmap ^bmp)
{
	int w = bmp->Width;
	int h = bmp->Height;
	Imaging::BitmapData ^Locked = bmp->LockBits(Rectangle(0, 0, w, h), Imaging::ImageLockMode::WriteOnly, Imaging::PixelFormat::Format32bppArgb);
	GetARGB((int*)(void*)Locked->Scan0, Locked->Stride, w, h);
	bmp->UnlockBits(Locked);
}

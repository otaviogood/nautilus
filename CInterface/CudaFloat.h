// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <builtin_types.h>
#include <device_launch_parameters.h>
#include <device_functions.h>
#include <cuda_texture_types.h>

struct ImageInfo
{
	unsigned int width, height, pitchInFloats;
	float invWidth, invHeight;
	float *d_buffer;
};

class CudaWrapperF
{
public:
	ImageInfo info;
	textureReference texRef;
	dim3 dimBlock;
	dim3 dimGrid;


public:
	CudaWrapperF(int width, int height);
	~CudaWrapperF();

	void Init(int width, int height);
	void SetTexRefDest();
	void SetAndBindTexRefSource0();
	void SetAndBindTexRefSource1();
	void SetAndBindTexRefSource2();

	void GetAsFloatArray(float *dest);
	void SetFromFloatArray(float *source);
	void SetFromFloatArrayStride(float *source);
	void Clear();
	void SetPixel(int x, int y, float val);
	void FillRectangle(int x, int y, int width, int height, float val);
	float GetPixel(int x, int y);
	//void Rotate(CudaWrapperF *dest, float angle);
	//void Scale(CudaWrapperF *dest, float scale, bool bilinear);
	//void ScaleBicubic(CudaWrapperF *dest, float scale);
	void BasicKernel2Image(CudaWrapperF *source0, CudaWrapperF *source1, int op);
	void BasicKernel1Const(CudaWrapperF *source0, float val, int op);
	void ConvertToARGB(int *dest, int destPitchInInts, int w, int h);
	void GenRandom(CudaWrapperF *randImage0, CudaWrapperF *randImage1, int randOffset);
	void GenPerlin(CudaWrapperF *randImage0, CudaWrapperF *randImage1, int randOffset, float scale);
	void FindExtents(int *boundingBoxResult);
	void FindExtentsAndCopy(CudaWrapperF *source0);
	void CopyFromScaledRegion(CudaWrapperF *source0, int *boundingBox);
	int Diff(CudaWrapperF *source0);
	void DiffMultiple(float **allRefPointers, int numElements, int *results);
	static void CudaLockMem(void *p, int size)
	{
		cudaError_t err = cudaHostRegister(p, size, 0);
		assert(err == cudaSuccess);
	}
	static void CudaUnlockMem(void *p)
	{
		cudaError_t err = cudaHostUnregister(p);
		assert(err == cudaSuccess);
	}
	void GenGaussian(float xPos, float yPos, float radius, float magnitude);
	void WaveGaussian(CudaWrapperF *destVel, CudaWrapperF *heightBuffer, CudaWrapperF *wallBuffer, CudaWrapperF *velBuffer);
	void GaussianBlurRadius(CudaWrapperF *source0, float radius);
	static CudaWrapperF *GenNormalizedGaussianKernel(float radius);
	void GaussianBlurFromKernel(CudaWrapperF *source0, CudaWrapperF *kernel, CudaWrapperF *tempImage);
	void FourierForward(CudaWrapperF *source0);
	void FourierInverse(CudaWrapperF *source0);
	void ComplexSplitToPhaseMagnitude(CudaWrapperF *destPhase, CudaWrapperF *destMag);
	void ComplexConjugate(CudaWrapperF * source0);
	void FindBrightestPoint(int *pos);
};


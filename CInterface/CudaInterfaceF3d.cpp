// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "CudaInterfaceF3d.h"

#include "CudaFloat3d.h"

CudaInterface::CudaImageF3d::CudaImageF3d(int width, int height, int depth)
{
	wrapper = new CudaWrapperF3d(width, height, depth);
}

CudaInterface::CudaImageF3d::!CudaImageF3d()
{
	delete wrapper;
}

CudaInterface::CudaImageF3d::CudaImageF3d(array<Byte> ^bufferAsBytes, int width, int height, int depth)
{
	wrapper = new CudaWrapperF3d(width, height, depth);
	assert(wrapper->info.pitchInFloats == width);	// if not, implement strided copy.
	pin_ptr<Byte> p = &bufferAsBytes[0];
	wrapper->SetFromFloatArray((float*)p);
}



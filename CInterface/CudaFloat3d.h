// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <builtin_types.h>
#include <device_launch_parameters.h>
#include <device_functions.h>
#include <cuda_texture_types.h>

struct ImageInfo
{
	unsigned int width, height, depth, pitchInFloats;
	float invWidth, invHeight, invDepth;
	float *d_buffer;
};

class CudaWrapperF3d
{
public:
	ImageInfo info;
	textureReference texRef;
	dim3 dimBlock;
	dim3 dimGrid;


public:
	CudaWrapperF3d(int width, int height, int depth);
	~CudaWrapperF3d();

	void Init(int width, int height, int depth);
	void SetTexRefDest();
	void SetAndBindTexRefSource0();
	void SetAndBindTexRefSource1();
	void SetAndBindTexRefSource2();

	void GetAsFloatArray(float *dest);
	void SetFromFloatArray(float *source);
	void SetFromFloatArrayStride(float *source);
	void Clear();
	void SetPixel(int x, int y, float val);
	float GetPixel(int x, int y);
};


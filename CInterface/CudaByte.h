// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <builtin_types.h>
#include <device_launch_parameters.h>
#include <device_functions.h>
#include <cuda_texture_types.h>

typedef unsigned char Byte;

struct ImageInfoB
{
	unsigned int width, height, pitchInBytes;
	float invWidth, invHeight;
	Byte *d_buffer;
};

class CudaWrapperB
{
public:
	ImageInfoB info;
	textureReference texRef;
	dim3 dimBlock;
	dim3 dimGrid;


public:
	CudaWrapperB(int width, int height);
	~CudaWrapperB();

	void Init(int width, int height);
	void SetTexRefDest();
	void SetAndBindTexRefSource0(ImageInfoB ii);
	void SetAndBindTexRefSource1(ImageInfoB ii);

	void GetAsByteArray(Byte *dest);
	void SetFromByteArray(Byte *source);
	void Clear();
	void SetPixel(int x, int y, Byte val);
	Byte GetPixel(int x, int y);
	void BasicKernel2Image(CudaWrapperB *source0, CudaWrapperB *source1, int op);
	void BasicKernel1Const(CudaWrapperB *source0, float val, int op);
	void ConvertToARGB(int *dest, int destPitchInInts, int w, int h);
	void GenRandom(CudaWrapperB *randImage0, CudaWrapperB *randImage1, int randOffset);
	//void GenPerlin(CudaWrapperB *randImage0, CudaWrapperB *randImage1, int randOffset, float scale);
	void FindExtents(int *boundingBoxResult);
	void FindExtentsAndCopy(CudaWrapperB *source0);
	void CopyFromScaledRegion(CudaWrapperB *source0, int *boundingBox);
};


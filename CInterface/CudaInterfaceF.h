// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#define CUDA_INTERFACE_F
#include <assert.h>
#include "CudaFloat.h"

using namespace System;
using namespace System::Drawing;
//using namespace System::IO;

namespace CudaInterface {

	public ref class CudaImageF
	{
		CudaWrapperF *wrapper;
		static CudaWrapperF *randImage0, *randImage1;
		static int randOffset;
public:
		CudaImageF()
		{
			wrapper = NULL;
		}
		// warning - this is a byte array containing floats
		CudaImageF(array<Byte> ^bufferAsBytes, int width, int height);
		CudaImageF(int width, int height);
		CudaImageF(String ^filename);
		CudaImageF(Bitmap ^bmp);
		static CudaImageF();
		!CudaImageF();	// finalizer to free graphics mem on GC http://msdn.microsoft.com/en-us/library/ms177197(v=vs.100).aspx
		inline int GetPitchInFloats()
		{
			return (wrapper->info.pitchInFloats);
		}
		inline int GetWidth()
		{
			return wrapper->info.width;
		}
		inline int GetHeight()
		{
			return wrapper->info.height;
		}

		inline void GetAsFloatArray(float *dest)
		{
			wrapper->GetAsFloatArray(dest);
		}
		inline void GetAsFloatArray(array<float> ^dest)
		{
			pin_ptr<float> p = &dest[0];
			wrapper->GetAsFloatArray(p);
		}
		inline void SetFromFloatArray(float *source)
		{
			wrapper->SetFromFloatArray(source);
		}
		// warning - this is a byte array containing floats
		inline void SetFromByteArray(array<Byte> ^source, int width, int height, int offsetInBytes)
		{
			if (wrapper->info.pitchInFloats == width)	// if not, implement strided copy.
			{
				pin_ptr<Byte> p = &source[offsetInBytes];
				wrapper->SetFromFloatArray((float*)p);
			}
			else
			{
				pin_ptr<Byte> p = &source[offsetInBytes];
				wrapper->SetFromFloatArrayStride((float*)p);
			}
		}
		inline static void CudaLockMem(void *p, int size)
		{
			CudaWrapperF::CudaLockMem(p, size);
		}
		inline static void CudaUnlockMem(void *p)
		{
			CudaWrapperF::CudaUnlockMem(p);
		}
		inline void SetPixel(int x, int y, float val)
		{
			wrapper->SetPixel(x, y, val);
		}
		inline void SetPixelSafe(int x, int y, float val)
		{
			if (x<0) return;
			if (y<0) return;
			if (x>=GetWidth()) return;
			if (y>=GetHeight()) return;
			wrapper->SetPixel(x, y, val);
		}
		inline void FillRectangle(int x, int y, int width, int height, float val)
		{
			wrapper->FillRectangle(x, y, width, height, val);
		}
		inline float GetPixel(int x, int y)
		{
			return wrapper->GetPixel(x, y);
		}
		inline float GetPixelSafe(int x, int y)
		{
			if (x<0) return 0;
			if (y<0) return 0;
			if (x>=GetWidth()) return 0;
			if (y>=GetHeight()) return 0;
			return wrapper->GetPixel(x, y);
		}
		inline void Clear()
		{
			wrapper->Clear();
		}

		// Always takes the size of the first parameter. For more control over sizes, us non-operator overloaded functions.
		static CudaImageF^ operator+(CudaImageF^ a, CudaImageF^ b)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 0);
			return tempImage;
		}
		static CudaImageF^ operator-(CudaImageF^ a, CudaImageF^ b)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 1);
			return tempImage;
		}
		static CudaImageF^ operator*(CudaImageF^ a, CudaImageF^ b)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 2);
			return tempImage;
		}
		static CudaImageF^ operator/(CudaImageF^ a, CudaImageF^ b)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 3);
			return tempImage;
		}
		static CudaImageF^ Pow_(CudaImageF^ a, CudaImageF^ b)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 4);
			return tempImage;
		}

		inline void Add(CudaImageF ^source0, CudaImageF ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 0);
		}
		inline void Sub(CudaImageF ^source0, CudaImageF ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 1);
		}
		inline void Mul(CudaImageF ^source0, CudaImageF ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 2);
		}
		inline void Div(CudaImageF ^source0, CudaImageF ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 3);
		}
		inline void Pow(CudaImageF ^source0, CudaImageF ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 4);
		}

		static CudaImageF^ operator+(CudaImageF^ a, float val)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 0);
			return tempImage;
		}
		static CudaImageF^ operator-(CudaImageF^ a, float val)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 1);
			return tempImage;
		}
		static CudaImageF^ operator*(CudaImageF^ a, float val)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 2);
			return tempImage;
		}
		static CudaImageF^ operator/(CudaImageF^ a, float val)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 3);
			return tempImage;
		}
		static CudaImageF^ Pow(CudaImageF^ a, float val)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 4);
			return tempImage;
		}
		static CudaImageF^ ScaleBicubic_(CudaImageF^ a, float val)
		{
			CudaImageF ^tempImage = gcnew CudaImageF((int)(a->GetWidth()*val), (int)(a->GetHeight()*val));
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 5);
			return tempImage;
		}
		static CudaImageF^ Scale_(CudaImageF^ a, float val)
		{
			CudaImageF ^tempImage = gcnew CudaImageF((int)(a->GetWidth()*val), (int)(a->GetHeight()*val));
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 6);
			return tempImage;
		}
		static CudaImageF^ ScaleBilinear_(CudaImageF^ a, float val)
		{
			CudaImageF ^tempImage = gcnew CudaImageF((int)(a->GetWidth()*val), (int)(a->GetHeight()*val));
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 7);
			return tempImage;
		}
		static CudaImageF^ Rotate_(CudaImageF^ a, float val)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 8);
			return tempImage;
		}

		inline void ScaleBicubic(CudaImageF ^source0, float val)
		{
			wrapper->BasicKernel1Const(source0->wrapper, val, 5);
		}
		inline void Scale(CudaImageF ^source0, float val)
		{
			wrapper->BasicKernel1Const(source0->wrapper, val, 6);
		}
		inline void ScaleBilinear(CudaImageF ^source0, float val)
		{
			wrapper->BasicKernel1Const(source0->wrapper, val, 7);
		}
		inline void Rotate(CudaImageF ^source0, float val)
		{
			wrapper->BasicKernel1Const(source0->wrapper, val, 8);
		}
		inline void TerrainRender(CudaImageF ^source0, float val)
		{
			wrapper->BasicKernel1Const(source0->wrapper, val, 9);
		}

		inline void GetARGB(int *p, int pitchInBytes, int width, int height)
		{
			wrapper->ConvertToARGB(p, pitchInBytes / 4, width, height);
		}
		void DrawToBitmap(Bitmap ^bmp);
		Bitmap ^Draw()
		{
			Bitmap ^temp = gcnew Bitmap(GetWidth(), GetHeight());
			DrawToBitmap(temp);
			return temp;
		}
		void Draw(Graphics ^g, int x, int y)
		{
			Bitmap ^bmp = Draw();
			g->DrawImageUnscaled(bmp, x, y);
//			bmp->Dispose();
		}
		void GenRandom()
		{
			wrapper->GenRandom(randImage0, randImage1, randOffset);
			randOffset += Math::Max(wrapper->info.width, wrapper->info.height) + 13;	// throw in an extra prime number for good luck. :)
		}
		void GenRandom(int seed)
		{
			wrapper->GenRandom(randImage0, randImage1, seed);
		}
		void GenPerlin(float scale, int seed)
		{
			wrapper->GenPerlin(randImage0, randImage1, seed, scale);
		}
		void FindExtents(array<int> ^boundingBoxResult)
		{
			pin_ptr<int> p = &boundingBoxResult[0];
			wrapper->FindExtents(p);
		}
		void FindExtentsAndCopy(CudaImageF ^source0)
		{
			wrapper->FindExtentsAndCopy(source0->wrapper);
		}
		inline void CopyFromScaledRegion(CudaImageF ^source0, array<int> ^boundingBox)
		{
			pin_ptr<int> p = &boundingBox[0];
			wrapper->CopyFromScaledRegion(source0->wrapper, p);
		}
		inline int Diff(CudaImageF ^source0)
		{
			assert(GetWidth() == source0->GetWidth());
			assert(GetHeight() == source0->GetHeight());
			return wrapper->Diff(source0->wrapper);
		}

		typedef float* devicePointer;
		inline array<int> ^DiffMultiple(array<CudaImageF^> ^allRefs, int numElements)
		{
			devicePointer *allRefPointers = new devicePointer[numElements];
			array<int> ^result = gcnew array<int>(numElements);
			for (int count = 0; count < numElements; count++)
			{
				assert(GetWidth() == allRefs[count]->GetWidth());
				assert(GetHeight() == allRefs[count]->GetHeight());
				assert(GetPitchInFloats() == allRefs[count]->GetPitchInFloats());
				allRefPointers[count] = allRefs[count]->wrapper->info.d_buffer;
			}
			pin_ptr<int> pOut = &result[0];
			wrapper->DiffMultiple(allRefPointers, numElements, pOut);
			delete[] allRefPointers;
			return result;
		}
		void GenerateGaussianKernel(float xPos, float yPos, float radius, float magnitude)
		{
			wrapper->GenGaussian(xPos, yPos, radius, magnitude);
		}
		void WaveGaussian(CudaImageF ^destVel, CudaImageF ^heightBuffer, CudaImageF ^wallBuffer, CudaImageF ^velBuffer)
		{
			assert(wallBuffer->GetHeight() == heightBuffer->GetHeight());
			wrapper->WaveGaussian(destVel->wrapper, heightBuffer->wrapper, wallBuffer->wrapper, velBuffer->wrapper);
		}
		static CudaImageF^ FourierForward(CudaImageF ^source0)
		{
			CudaImageF ^tempImage = gcnew CudaImageF((int)(source0->GetWidth()+2), (int)(source0->GetHeight()));
			tempImage->wrapper->FourierForward(source0->wrapper);
			return tempImage;
		}
		static CudaImageF^ FourierInverse(CudaImageF ^source0)
		{
			CudaImageF ^tempImage = gcnew CudaImageF((int)(source0->GetWidth()-2), (int)(source0->GetHeight()));
			tempImage->wrapper->FourierInverse(source0->wrapper);
			return tempImage;
		}
		void ComplexSplitToPhaseMagnitude(CudaImageF ^%destPhase, CudaImageF ^%destMag)
		{
			destPhase = gcnew CudaImageF(GetWidth() / 2, GetHeight());
			destMag = gcnew CudaImageF(GetWidth() / 2, GetHeight());
			wrapper->ComplexSplitToPhaseMagnitude(destPhase->wrapper, destMag->wrapper);
		}
		static CudaImageF^ ComplexConjugate(CudaImageF ^source0)
		{
			CudaImageF ^tempImage = gcnew CudaImageF((int)(source0->GetWidth()), (int)(source0->GetHeight()));
			tempImage->wrapper->ComplexConjugate(source0->wrapper);
			return tempImage;
		}
		static CudaImageF^ ComplexMultiply(CudaImageF^ a, CudaImageF^ b)
		{
			CudaImageF ^tempImage = gcnew CudaImageF(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 6);
			return tempImage;
		}
		void ComplexBandPass(int startFreq, int endFreqInclusive)
		{
			FillRectangle(0, 0, startFreq, GetHeight(), 0);
			FillRectangle(endFreqInclusive + 1,0, GetWidth(), GetHeight(), 0);
			FillRectangle(0, 0, GetWidth(), startFreq, 0);
			FillRectangle(0, endFreqInclusive + 1, GetWidth(), GetHeight(), 0);
		}
		void FindBrightestPoint(array<int> ^pos)
		{
			assert(0);//cuda kernel not implemented
			pin_ptr<int> p = &pos[0];
			wrapper->FindBrightestPoint(p);
		}


	protected:
		static void GenInitialRandom(CudaWrapperF *dest, int seed)
		{
			Random ^r = gcnew Random(seed);
			int numPixels = dest->info.pitchInFloats * dest->info.height;
			float *fValues = new float[numPixels];
			for (int count = 0; count < numPixels; count++)
			{
				fValues[count] = (float)r->Next(65536);
			}
			dest->SetFromFloatArray(fValues);
			delete fValues;
		}

	};
}

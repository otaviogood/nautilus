// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include <assert.h>
#include "CudaFloat3d.h"

using namespace System;
using namespace System::Drawing;
//using namespace System::IO;

namespace CudaInterface {

	public ref class CudaImageF3d
	{
		CudaWrapperF3d *wrapper;
public:
		CudaImageF3d()
		{
			wrapper = NULL;
		}
		// warning - this is a byte array containing floats
		CudaImageF3d(array<Byte> ^bufferAsBytes, int width, int height, int depth);
		CudaImageF3d(int width, int height, int depth);
		!CudaImageF3d();	// finalizer to free graphics mem on GC http://msdn.microsoft.com/en-us/library/ms177197(v=vs.100).aspx
		inline int GetPitchInFloats()
		{
			return (wrapper->info.pitchInFloats);
		}
		inline int GetWidth()
		{
			return wrapper->info.width;
		}
		inline int GetHeight()
		{
			return wrapper->info.height;
		}

		inline void GetAsFloatArray(float *dest)
		{
			wrapper->GetAsFloatArray(dest);
		}
		inline void SetFromFloatArray(float *source)
		{
			wrapper->SetFromFloatArray(source);
		}
		// warning - this is a byte array containing floats
		inline void SetFromByteArray(array<Byte> ^source, int width, int height, int offsetInBytes)
		{
			if (wrapper->info.pitchInFloats == width)	// if not, implement strided copy.
			{
				pin_ptr<Byte> p = &source[offsetInBytes];
				wrapper->SetFromFloatArray((float*)p);
			}
			else
			{
				pin_ptr<Byte> p = &source[offsetInBytes];
				wrapper->SetFromFloatArrayStride((float*)p);
			}
		}
		inline void SetPixel(int x, int y, float val)
		{
			wrapper->SetPixel(x, y, val);
		}
		inline void SetPixelSafe(int x, int y, float val)
		{
			if (x<0) return;
			if (y<0) return;
			if (x>=GetWidth()) return;
			if (y>=GetHeight()) return;
			wrapper->SetPixel(x, y, val);
		}
		inline float GetPixel(int x, int y)
		{
			return wrapper->GetPixel(x, y);
		}
		inline float GetPixelSafe(int x, int y)
		{
			if (x<0) return 0;
			if (y<0) return 0;
			if (x>=GetWidth()) return 0;
			if (y>=GetHeight()) return 0;
			return wrapper->GetPixel(x, y);
		}
		inline void Clear()
		{
			wrapper->Clear();
		}


	};
}

// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#ifndef BASIC_CU3D
#define BASIC_CU3D

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <builtin_types.h>
#include <device_launch_parameters.h>
#include <device_functions.h>
#include <assert.h>

#include "cutil_math.h"
#include "CudaFloat3d.h"

#ifdef _DEBUG
#define ASSERT(x) assert(x)
#else
#define ASSERT(x) assert(x)
//#define ASSERT(x) ;
#endif

texture<float, cudaTextureType2D, cudaReadModeElementType> texSource0;
texture<float, cudaTextureType2D, cudaReadModeElementType> texSource1;
texture<float, cudaTextureType2D, cudaReadModeElementType> texSource2;

__device__ void SetPixelF3d(ImageInfo ii, int x, int y, float val)
{
	ii.d_buffer[y * ii.pitchInFloats + x] = val;
}
__device__ void SetPixelF3d(float* const destBuffer, const int destPitchInFloats, const int x, const int y, const float val)
{
	destBuffer[y * destPitchInFloats + x] = val;
}



// ------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------

CudaWrapperF3d::CudaWrapperF3d(int width, int height, int depth)
{
	Init(width, height, depth);
}

CudaWrapperF3d::~CudaWrapperF3d()
{
	if (info.d_buffer) cudaFree(info.d_buffer);
}

static int *d_scratchPad = NULL;

void CudaWrapperF3d::Init(int width, int height, int depth)
{
	info.width = width;
	info.height = height;
	info.depth = depth;
	info.invWidth = 1.0f / width;
	info.invHeight = 1.0f / height;
	info.invDepth = 1.0f / depth;
	size_t pitchInBytes;
	cudaError_t err = cudaMallocPitch((void **)&info.d_buffer, &pitchInBytes, width * sizeof(float), height * depth);
	assert(err != cudaErrorMemoryAllocation);
	info.pitchInFloats = pitchInBytes/4;
	texRef.addressMode[0] = cudaAddressModeWrap;
	texRef.addressMode[1] = cudaAddressModeWrap;
	texRef.addressMode[2] = cudaAddressModeWrap;
	texRef.filterMode = cudaFilterModeLinear;
	texRef.normalized = true;
	dimBlock = dim3(16, 8, 1);
	dimGrid = dim3(info.width / dimBlock.x + (((info.width % dimBlock.x)!=0)?1:0), info.height / dimBlock.y + (((info.height % dimBlock.y)!=0)?1:0), 1);
	if (!d_scratchPad)
	{
		cudaError_t err = cudaMalloc(&d_scratchPad, 65536*4);
		assert(err != cudaErrorMemoryAllocation);
	}
}

void CudaWrapperF3d::SetAndBindTexRefSource0()
{
	texSource0.addressMode[0] = texRef.addressMode[0];
	texSource0.addressMode[1] = texRef.addressMode[1];
	texSource0.filterMode = texRef.filterMode;
	texSource0.normalized = texRef.normalized;
	size_t offset;
	cudaError_t err = cudaBindTexture2D(&offset, texSource0, info.d_buffer, cudaCreateChannelDesc<float>(), info.width, info.height, info.pitchInFloats*4);
	ASSERT(err != cudaErrorInvalidValue);
	ASSERT(err != cudaErrorInvalidDevicePointer);
	ASSERT(err != cudaErrorInvalidTexture);
	ASSERT(err == cudaSuccess);
	ASSERT(offset == 0);
}

void CudaWrapperF3d::SetAndBindTexRefSource1()
{
	texSource1.addressMode[0] = texRef.addressMode[0];
	texSource1.addressMode[1] = texRef.addressMode[1];
	texSource1.filterMode = texRef.filterMode;
	texSource1.normalized = texRef.normalized;
	size_t offset;
	cudaError_t err = cudaBindTexture2D(&offset, texSource1, info.d_buffer, cudaCreateChannelDesc<float>(), info.width, info.height, info.pitchInFloats*4);
	ASSERT(offset == 0);
}

void CudaWrapperF3d::SetAndBindTexRefSource2()
{
	texSource2.addressMode[0] = texRef.addressMode[0];
	texSource2.addressMode[1] = texRef.addressMode[1];
	texSource2.filterMode = texRef.filterMode;
	texSource2.normalized = texRef.normalized;
	size_t offset;
	cudaError_t err = cudaBindTexture2D(&offset, texSource2, info.d_buffer, cudaCreateChannelDesc<float>(), info.width, info.height, info.pitchInFloats*4);
	ASSERT(offset == 0);
}

void CudaWrapperF3d::GetAsFloatArray(float *dest)
{
	cudaMemcpy2D(dest, info.pitchInFloats*4, info.d_buffer, info.pitchInFloats*4, info.width * sizeof(float), info.height, cudaMemcpyDeviceToHost);
}

void CudaWrapperF3d::SetFromFloatArray(float *source)
{
	cudaMemcpy2D(info.d_buffer, info.pitchInFloats*4, source, info.pitchInFloats*4, info.width * sizeof(float), info.height, cudaMemcpyHostToDevice);
}

void CudaWrapperF3d::SetFromFloatArrayStride(float *source)
{
	cudaMemcpy2D(info.d_buffer, info.pitchInFloats*4, source, info.width*4, info.width * sizeof(float), info.height, cudaMemcpyHostToDevice);
}

void CudaWrapperF3d::SetPixel(int x, int y, float val)
{
	//float* pElement = (float*)((char*)d_buffer + y * m_pitchInBytes) + x;
	float* pElement = (float*)(info.d_buffer + y * info.pitchInFloats) + x;
	// probably shouldn't do Async since "val" will go out of scope immediately.
	cudaMemcpy2D(pElement, info.pitchInFloats*4, &val, 4, 4, 1, cudaMemcpyHostToDevice);
}

float CudaWrapperF3d::GetPixel(int x, int y)
{
	//float* pElement = (float*)((char*)d_buffer + y * m_pitchInBytes) + x;
	float* pElement = (float*)(info.d_buffer + y * info.pitchInFloats) + x;
	float result;
	cudaMemcpy(&result, pElement, 4,cudaMemcpyDeviceToHost);
	return result;
}

void CudaWrapperF3d::Clear()
{
	cudaMemset2D(info.d_buffer, info.pitchInFloats*4, 0, info.width * sizeof(float), info.height);
}




#endif //BASIC_CU3D

// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#include <assert.h>
#include "CudaByte.h"

using namespace System;
using namespace System::Drawing;
//using namespace System::IO;

namespace CudaInterface {

	public ref class CudaImageB
	{
		CudaWrapperB *wrapper;
		static CudaWrapperB *randImage0, *randImage1;
		static int randOffset;
public:
		CudaImageB()
		{
			wrapper = NULL;
		}
		CudaImageB(array<System::Byte> ^bufferAsBytes, int width, int height);
		CudaImageB(int width, int height);
		CudaImageB(String ^filename);
		CudaImageB(Bitmap ^bmp);
		static CudaImageB();
		!CudaImageB();	// finalizer to free graphics mem on GC http://msdn.microsoft.com/en-us/library/ms177197(v=vs.100).aspx
		inline int GetPitchInBytes()
		{
			return (wrapper->info.pitchInBytes);
		}
		inline int GetWidth()
		{
			return wrapper->info.width;
		}
		inline int GetHeight()
		{
			return wrapper->info.height;
		}

		inline void GetAsByteArray(System::Byte *dest)
		{
			wrapper->GetAsByteArray(dest);
		}
		inline void SetFromByteArray(System::Byte *source)
		{
			wrapper->SetFromByteArray(source);
		}
		inline void SetFromByteArray(array<System::Byte> ^source, int width, int height, int offsetInBytes)
		{
			if (wrapper->info.pitchInBytes == width)
			{
				pin_ptr<System::Byte> p = &source[offsetInBytes];
				wrapper->SetFromByteArray((System::Byte*)p);
			}
			else
			{
				// if not, implement strided copy.
				assert(0);
			}
		}
		inline void SetPixel(int x, int y, System::Byte val)
		{
			wrapper->SetPixel(x, y, val);
		}
		inline System::Byte GetPixel(int x, int y)
		{
			return wrapper->GetPixel(x, y);
		}
		inline void Clear()
		{
			wrapper->Clear();
		}

		// Always takes the size of the first parameter. For more control over sizes, us non-operator overloaded functions.
		static CudaImageB^ operator+(CudaImageB^ a, CudaImageB^ b)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 0);
			return tempImage;
		}
		static CudaImageB^ operator-(CudaImageB^ a, CudaImageB^ b)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 1);
			return tempImage;
		}
		static CudaImageB^ operator*(CudaImageB^ a, CudaImageB^ b)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 2);
			return tempImage;
		}
		static CudaImageB^ operator/(CudaImageB^ a, CudaImageB^ b)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 3);
			return tempImage;
		}
		static CudaImageB^ Pow_(CudaImageB^ a, CudaImageB^ b)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel2Image(a->wrapper, b->wrapper, 4);
			return tempImage;
		}

		inline void Add(CudaImageB ^source0, CudaImageB ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 0);
		}
		inline void Sub(CudaImageB ^source0, CudaImageB ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 1);
		}
		inline void Mul(CudaImageB ^source0, CudaImageB ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 2);
		}
		inline void Div(CudaImageB ^source0, CudaImageB ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 3);
		}
		inline void Pow(CudaImageB ^source0, CudaImageB ^source1)
		{
			wrapper->BasicKernel2Image(source0->wrapper, source1->wrapper, 4);
		}

		static CudaImageB^ operator+(CudaImageB^ a, System::Byte val)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 0);
			return tempImage;
		}
		static CudaImageB^ operator-(CudaImageB^ a, System::Byte val)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 1);
			return tempImage;
		}
		static CudaImageB^ operator*(CudaImageB^ a, System::Byte val)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 2);
			return tempImage;
		}
		static CudaImageB^ operator/(CudaImageB^ a, System::Byte val)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 3);
			return tempImage;
		}
		static CudaImageB^ Pow(CudaImageB^ a, System::Byte val)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 4);
			return tempImage;
		}
		static CudaImageB^ ScaleBicubic_(CudaImageB^ a, float val)
		{
			CudaImageB ^tempImage = gcnew CudaImageB((int)(a->GetWidth()*val), (int)(a->GetHeight()*val));
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 5);
			return tempImage;
		}
		static CudaImageB^ Scale_(CudaImageB^ a, float val)
		{
			CudaImageB ^tempImage = gcnew CudaImageB((int)(a->GetWidth()*val), (int)(a->GetHeight()*val));
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 6);
			return tempImage;
		}
		static CudaImageB^ ScaleBilinear_(CudaImageB^ a, float val)
		{
			CudaImageB ^tempImage = gcnew CudaImageB((int)(a->GetWidth()*val), (int)(a->GetHeight()*val));
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 7);
			return tempImage;
		}
		static CudaImageB^ Rotate_(CudaImageB^ a, float val)
		{
			CudaImageB ^tempImage = gcnew CudaImageB(a->GetWidth(), a->GetHeight());
			tempImage->wrapper->BasicKernel1Const(a->wrapper, val, 8);
			return tempImage;
		}

		inline void ScaleBicubic(CudaImageB ^source0, float val)
		{
			wrapper->BasicKernel1Const(source0->wrapper, val, 5);
		}
		inline void Scale(CudaImageB ^source0, float val)
		{
			wrapper->BasicKernel1Const(source0->wrapper, val, 6);
		}
		inline void ScaleBilinear(CudaImageB ^source0, float val)
		{
			wrapper->BasicKernel1Const(source0->wrapper, val, 7);
		}
		inline void Rotate(CudaImageB ^source0, float val)
		{
			wrapper->BasicKernel1Const(source0->wrapper, val, 8);
		}

		inline void GetARGB(int *p, int pitchInBytes, int width, int height)
		{
			wrapper->ConvertToARGB(p, pitchInBytes / 4, width, height);
		}
		void DrawToBitmap(Bitmap ^bmp);
		void GenRandom()
		{
			wrapper->GenRandom(randImage0, randImage1, randOffset);
			randOffset += Math::Max(wrapper->info.width, wrapper->info.height) + 13;	// throw in an extra prime number for good luck. :)
		}
		void GenRandom(int seed)
		{
			wrapper->GenRandom(randImage0, randImage1, seed);
		}
		//void GenPerlin(float scale, int seed)
		//{
		//	wrapper->GenPerlin(randImage0, randImage1, seed, scale);
		//}
		void FindExtents(array<int> ^boundingBoxResult)
		{
			pin_ptr<int> p = &boundingBoxResult[0];
			wrapper->FindExtents(p);
		}
		void FindExtentsAndCopy(CudaImageB ^source0)
		{
			wrapper->FindExtentsAndCopy(source0->wrapper);
		}
		inline void CopyFromScaledRegion(CudaImageB ^source0, array<int> ^boundingBox)
		{
			pin_ptr<int> p = &boundingBox[0];
			wrapper->CopyFromScaledRegion(source0->wrapper, p);
		}
	protected:
		static void GenInitialRandom(CudaWrapperB *dest, int seed)
		{
			Random ^r = gcnew Random(seed);
			int numPixels = dest->info.pitchInBytes * dest->info.height;
			System::Byte *bValues = new System::Byte[numPixels];
			for (int count = 0; count < numPixels; count++)
			{
				bValues[count] = (System::Byte)r->Next(256);
			}
			dest->SetFromByteArray(bValues);
			delete bValues;
		}

	};
}

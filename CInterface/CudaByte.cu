// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <builtin_types.h>
#include <device_launch_parameters.h>
#include <device_functions.h>
#include <assert.h>

#include "CudaByte.h"

texture<Byte, cudaTextureType2D, cudaReadModeElementType> texSourceB0;
texture<Byte, cudaTextureType2D, cudaReadModeElementType> texSourceB1;

__device__ void SetPixelB(ImageInfoB ii, int x, int y, Byte val)
{
	ii.d_buffer[y * ii.pitchInBytes + x] = val;
}
__device__ void SetPixelB(Byte* const destBuffer, const int destPitchInBytes, const int x, const int y, const Byte val)
{
	destBuffer[y * destPitchInBytes + x] = val;
}

__global__ void rotateKernelTexB(ImageInfoB dest, ImageInfoB source, float theta)
{
    // calculate normalized texture coordinates
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u = (x+0.5f) * source.invWidth;
    float v = (y+0.5f) * source.invHeight;

    // transform coordinates
    u -= 0.5f;
    v -= 0.5f;
	float sTemp;
	float cTemp;
	__sincosf(theta, &sTemp, &cTemp);
    float tu = u*cTemp - v*sTemp + 0.5f;
    float tv = v*cTemp + u*sTemp + 0.5f;

	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, tu, tv));
}

__global__ void CudaConvertByteToARGB(int* dest, int destPitchInInts, int height, ImageInfoB source)
{
    // calculate normalized texture coordinates
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= destPitchInInts) return;
	if (y >= height) return;

    float u = x;// / (float) source.width;
    float v = y;// / (float) source.height;

	Byte pix = 0;
	if ((u < source.width) && (v < source.height)) pix = source.d_buffer[(int)v * source.pitchInBytes + (int)u];
	int positive = max(min(pix, 255), 0);
	dest[y * destPitchInInts + x] = 0xff000000 | (positive << 16) + (positive << 8) + (positive);
}

__global__ void GenRandomKernelTexB(float invW0, float invH0, float invW1, float invH1, int offset, ImageInfoB dest)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

	float u0 = (x+0.5f+offset) * invW0;
    float v0 = (y+0.5f+offset) * invH0;
	float u1 = (x+0.5f+offset) * invW1;
    float v1 = (y+0.5f+offset) * invH1;
	int a = (int)tex2D(texSourceB0, u0, v0);
	int b = (int)tex2D(texSourceB1, u1, v1);
	int c = a ^ b;

	SetPixelB(dest, x, y, c);
}

//__device__ float GetPixelPerlin(ImageInfoB info0, ImageInfoB info1, float x, float y)
//{
//	// http://www.lems.brown.edu/vision/vxl_doc/html/core/vil/html/
//	float p1x = (int)x;
//	float normx = x - p1x;
//	float p1y = (int)y;
//	float normy = y - p1y;
//
//	// like bilinear interpolation, use separability.
//	// the s's are for the x-direction and the t's for the y-direction.
//	float s0 = ((2 - normx) * normx - 1) * normx;    // -1
//	float s1 = (3 * normx - 5) * normx * normx + 2;    //  0
//	float s2 = ((4 - 3 * normx) * normx + 1) * normx;  // +1
//	float s3 = (normx - 1) * normx * normx;        // +2
//
//	float t0 = ((2 - normy) * normy - 1) * normy;
//	float t1 = (3 * normy - 5) * normy * normy + 2;
//	float t2 = ((4 - 3 * normy) * normy + 1) * normy;
//	float t3 = (normy - 1) * normy * normy;
//
//	float invWidth0 = info0.invWidth;
//	float invHeight0 = info0.invHeight;
//	float invWidth02 = invWidth0 * 2;
//	float invHeight02 = invHeight0 * 2;
//	float invWidth1 = info1.invWidth;
//	float invHeight1 = info1.invHeight;
//	float invWidth12 = invWidth1 * 2;
//	float invHeight12 = invHeight1 * 2;
//
//	float p1x0 = p1x * invWidth0;
//	float p1y0 = p1y * invHeight0;
//	float p1x1 = p1x * invWidth1;
//	float p1y1 = p1y * invHeight1;
//
//	float xi0 =
//		((int)tex2D(texSourceB0, p1x0 - invWidth0, p1y0 - invHeight0) ^ (int)tex2D(texSourceB1, p1x1 - invWidth1, p1y1 - invHeight1) ) * s0 +
//		((int)tex2D(texSourceB0, p1x0 + 0, p1y0 - invHeight0) ^ (int)tex2D(texSourceB1, p1x1 + 0, p1y1 - invHeight1)) * s1 +
//		((int)tex2D(texSourceB0, p1x0 + invWidth0, p1y0 - invHeight0) ^ (int)tex2D(texSourceB1, p1x1 + invWidth1, p1y1 - invHeight1)) * s2 +
//		((int)tex2D(texSourceB0, p1x0 + invWidth02, p1y0 - invHeight0) ^ (int)tex2D(texSourceB1, p1x1 + invWidth12, p1y1 - invHeight1)) * s3;
//	float xi1 =
//		((int)tex2D(texSourceB0, p1x0 - invWidth0, p1y0 + 0) ^ (int)tex2D(texSourceB1, p1x1 - invWidth1, p1y1 + 0)) * s0 +
//		((int)tex2D(texSourceB0, p1x0 + 0, p1y0 + 0) ^ (int)tex2D(texSourceB1, p1x1 + 0, p1y1 + 0)) * s1+
//		((int)tex2D(texSourceB0, p1x0 + invWidth0, p1y0 + 0) ^ (int)tex2D(texSourceB1, p1x1 + invWidth1, p1y1 + 0)) * s2 +
//		((int)tex2D(texSourceB0, p1x0 + invWidth02, p1y0 + 0) ^ (int)tex2D(texSourceB1, p1x1 + invWidth12, p1y1 + 0)) * s3;
//	float xi2 =
//		((int)tex2D(texSourceB0, p1x0 - invWidth0, p1y0 + invHeight0) ^ (int)tex2D(texSourceB1, p1x1 - invWidth1, p1y1 + invHeight1)) * s0 +
//		((int)tex2D(texSourceB0, p1x0 + 0, p1y0 + invHeight0) ^ (int)tex2D(texSourceB1, p1x1 + 0, p1y1 + invHeight1)) * s1 +
//		((int)tex2D(texSourceB0, p1x0 + invWidth0, p1y0 + invHeight0) ^ (int)tex2D(texSourceB1, p1x1 + invWidth1, p1y1 + invHeight1)) * s2 +
//		((int)tex2D(texSourceB0, p1x0 + invWidth02, p1y0 + invHeight0) ^ (int)tex2D(texSourceB1, p1x1 + invWidth12, p1y1 + invHeight1)) * s3;
//	float xi3 =
//		((int)tex2D(texSourceB0, p1x0 - invWidth0, p1y0 + invHeight02) ^ (int)tex2D(texSourceB1, p1x1 - invWidth1, p1y1 + invHeight12)) * s0 +
//		((int)tex2D(texSourceB0, p1x0 + 0, p1y0 + invHeight02) ^ (int)tex2D(texSourceB1, p1x1 + 0, p1y1 + invHeight12)) * s1 +
//		((int)tex2D(texSourceB0, p1x0 + invWidth0, p1y0 + invHeight02) ^ (int)tex2D(texSourceB1, p1x1 + invWidth1, p1y1 + invHeight12)) * s2 +
//		((int)tex2D(texSourceB0, p1x0 + invWidth02, p1y0 + invHeight02) ^ (int)tex2D(texSourceB1, p1x1 + invWidth12, p1y1 + invHeight12)) * s3;
//
//	float val = (xi0 * t0 + xi1 * t1 + xi2 * t2 + xi3 * t3) * 0.25f;
//
//	return val;
//}

//__global__ void GenPerlinKernelTex(ImageInfoB rand0, ImageInfoB rand1, int offset, float invScale, ImageInfoB dest)
//{
//    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
//    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
//	if (x >= dest.width) return;
//	if (y >= dest.height) return;
//
//	float u0 = (x+0.5f+offset) * invScale;
//    float v0 = (y+0.5f+offset) * invScale;
//
//	SetPixel(dest, x, y, GetPixelPerlin(rand0, rand1, u0, v0));
//}

__device__ Byte GetPixelBicubicB(ImageInfoB info, float x, float y)
{
	// http://www.lems.brown.edu/vision/vxl_doc/html/core/vil/html/
	float p1x = (int)x;
	float normx = x - p1x;
	float p1y = (int)y;
	float normy = y - p1y;

	// like bilinear interpolation, use separability.
	// the s's are for the x-direction and the t's for the y-direction.
	float s0 = ((2 - normx) * normx - 1) * normx;    // -1
	float s1 = (3 * normx - 5) * normx * normx + 2;    //  0
	float s2 = ((4 - 3 * normx) * normx + 1) * normx;  // +1
	float s3 = (normx - 1) * normx * normx;        // +2

	float t0 = ((2 - normy) * normy - 1) * normy;
	float t1 = (3 * normy - 5) * normy * normy + 2;
	float t2 = ((4 - 3 * normy) * normy + 1) * normy;
	float t3 = (normy - 1) * normy * normy;

	float invWidth = info.invWidth;
	float invHeight = info.invHeight;
	float invWidth2 = invWidth * 2;
	float invHeight2 = invHeight * 2;

	p1x *= invWidth;
	p1y *= invHeight;

	float xi0 = tex2D(texSourceB0, p1x - invWidth, p1y - invHeight) * s0		+ tex2D(texSourceB0, p1x + 0, p1y - invHeight) * s1	+ tex2D(texSourceB0, p1x + invWidth, p1y - invHeight) * s2	+ tex2D(texSourceB0, p1x + invWidth2, p1y - invHeight) * s3;
	float xi1 = tex2D(texSourceB0, p1x - invWidth, p1y + 0) * s0				+ tex2D(texSourceB0, p1x + 0, p1y + 0) * s1			+ tex2D(texSourceB0, p1x + invWidth, p1y + 0) * s2			+ tex2D(texSourceB0, p1x + invWidth2, p1y + 0) * s3;
	float xi2 = tex2D(texSourceB0, p1x - invWidth, p1y + invHeight) * s0		+ tex2D(texSourceB0, p1x + 0, p1y + invHeight) * s1	+ tex2D(texSourceB0, p1x + invWidth, p1y + invHeight) * s2	+ tex2D(texSourceB0, p1x + invWidth2, p1y + invHeight) * s3;
	float xi3 = tex2D(texSourceB0, p1x - invWidth, p1y + invHeight2) * s0	+ tex2D(texSourceB0, p1x + 0, p1y + invHeight2) * s1	+ tex2D(texSourceB0, p1x + invWidth, p1y + invHeight2) * s2	+ tex2D(texSourceB0, p1x + invWidth2, p1y + invHeight2) * s3;
	//float xi0 = tex2D(texSourceB0, p1x - 1, p1y - 1) * s0 + tex2D(texSourceB0, p1x + 0, p1y - 1) * s1 + tex2D(texSourceB0, p1x + 1, p1y - 1) * s2 + tex2D(texSourceB0, p1x + 2, p1y - 1) * s3;
	//float xi1 = tex2D(texSourceB0, p1x - 1, p1y + 0) * s0 + tex2D(texSourceB0, p1x + 0, p1y + 0) * s1 + tex2D(texSourceB0, p1x + 1, p1y + 0) * s2 + tex2D(texSourceB0, p1x + 2, p1y + 0) * s3;
	//float xi2 = tex2D(texSourceB0, p1x - 1, p1y + 1) * s0 + tex2D(texSourceB0, p1x + 0, p1y + 1) * s1 + tex2D(texSourceB0, p1x + 1, p1y + 1) * s2 + tex2D(texSourceB0, p1x + 2, p1y + 1) * s3;
	//float xi3 = tex2D(texSourceB0, p1x - 1, p1y + 2) * s0 + tex2D(texSourceB0, p1x + 0, p1y + 2) * s1 + tex2D(texSourceB0, p1x + 1, p1y + 2) * s2 + tex2D(texSourceB0, p1x + 2, p1y + 2) * s3;

	float val = (xi0 * t0 + xi1 * t1 + xi2 * t2 + xi3 * t3) * 0.25f;

	return (Byte)val;
}

__global__ void scaleBicubicKernelTexB(ImageInfoB dest, ImageInfoB source, float invScale)
{
    // calculate normalized texture coordinates
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, GetPixelBicubicB(source, (x+0.5f)*invScale, (y+0.5f)*invScale));
}

__global__ void ScaleKernelB(ImageInfoB dest, ImageInfoB source, float invScale)
{
    // calculate normalized texture coordinates
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

	float u = (x+0.5f) * source.invWidth;
	float v = (y+0.5f) * source.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u*invScale, v*invScale));
}

__global__ void AddKernelB(ImageInfoB dest, ImageInfoB source0, ImageInfoB source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u0, v0) + tex2D(texSourceB1, u1, v1));
}

__global__ void SubKernelB(ImageInfoB dest, ImageInfoB source0, ImageInfoB source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u0, v0) - tex2D(texSourceB1, u1, v1));
}

__global__ void MulKernelB(ImageInfoB dest, ImageInfoB source0, ImageInfoB source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u0, v0) * tex2D(texSourceB1, u1, v1));
}

__global__ void DivKernelB(ImageInfoB dest, ImageInfoB source0, ImageInfoB source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u0, v0) / tex2D(texSourceB1, u1, v1));
}

__global__ void PowKernelB(ImageInfoB dest, ImageInfoB source0, ImageInfoB source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, __powf(tex2D(texSourceB0, u0, v0), tex2D(texSourceB1, u1, v1)));
}

// Use Add for Subtract.
__global__ void AddConstKernelB(ImageInfoB dest, ImageInfoB source0, Byte val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u0, v0) + val);
}

__global__ void MulConstKernelB(ImageInfoB dest, ImageInfoB source0, Byte val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u0, v0) * val);
}

__global__ void DivConstKernelB(ImageInfoB dest, ImageInfoB source0, Byte val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u0, v0) / val);
}

__global__ void PowConstKernelB(ImageInfoB dest, ImageInfoB source0, Byte val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, __powf(tex2D(texSourceB0, u0, v0), val));
}

__global__ void FindExtentsKernelB(int* dest, ImageInfoB source0)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= source0.width) return;
	if (y >= source0.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	Byte pix = tex2D(texSourceB0, u0, v0);
	if (pix != 0)
	{
		atomicMin(&(dest[0]), x);
		atomicMin(&(dest[1]), y);
		atomicMax(&(dest[2]), x);
		atomicMax(&(dest[3]), y);
	}
}

__global__ void FindExtentsKernelBrokenB(int* dest, ImageInfoB source0)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= source0.width) return;
	if (y >= source0.height) return;

	__shared__ int tempBounds[4];

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	float pix = tex2D(texSourceB0, u0, v0);
	if (pix != 0)
	{
		atomicMin(&(tempBounds[0]), x);
		atomicMin(&(tempBounds[1]), y);
		atomicMax(&(tempBounds[2]), x);
		atomicMax(&(tempBounds[3]), y);
	}

	__syncthreads();
	//for (int e = 0; e < BLOCK_SIZE; ++e)
}

__global__ void CopyFromScaledRegionKernelB(ImageInfoB dest, ImageInfoB source0, int minx, int miny, int maxx, int maxy)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * dest.invWidth;
    float v0 = (y+0.5f) * dest.invHeight;
	u0 = minx * source0.invWidth + (maxx-minx)*source0.invWidth *u0;
	v0 = miny * source0.invHeight+ (maxy-miny)*source0.invHeight*v0;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u0, v0));
}

__global__ void CopyFromScaledRegionKernelB(ImageInfoB dest, ImageInfoB source0, int *bounds)// int minx, int miny, int maxx, int maxy)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;
	float minx = bounds[0];
	float miny = bounds[1];
	float maxx = bounds[2];
	float maxy = bounds[3];

    float u0 = (x+0.5f) * dest.invWidth;
    float v0 = (y+0.5f) * dest.invHeight;
	u0 = minx * source0.invWidth + (maxx-minx)*source0.invWidth *u0;
	v0 = miny * source0.invHeight+ (maxy-miny)*source0.invHeight*v0;
	SetPixelB(dest.d_buffer, dest.pitchInBytes, x, y, tex2D(texSourceB0, u0, v0));
}



// ------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------

CudaWrapperB::CudaWrapperB(int width, int height)
{
	Init(width, height);
}

CudaWrapperB::~CudaWrapperB()
{
	if (info.d_buffer) cudaFree(info.d_buffer);
}

static int *d_scratchPad2 = NULL;

void CudaWrapperB::Init(int width, int height)
{
	info.width = width;
	info.height = height;
	info.invWidth = 1.0f / width;
	info.invHeight = 1.0f / height;
	size_t pitchInBytes;
	cudaMallocPitch((void **)&info.d_buffer, &pitchInBytes, width * sizeof(Byte), height);
	info.pitchInBytes = pitchInBytes;
	texRef.addressMode[0] = cudaAddressModeWrap;
	texRef.addressMode[1] = cudaAddressModeWrap;
	texRef.filterMode = cudaFilterModeLinear;
	texRef.normalized = true;
	dimBlock = dim3(16, 8, 1);
	dimGrid = dim3(info.width / dimBlock.x + (((info.width % dimBlock.x)!=0)?1:0), info.height / dimBlock.y + (((info.height % dimBlock.y)!=0)?1:0), 1);
	if (!d_scratchPad2)
	{
		cudaError_t err = cudaMalloc(&d_scratchPad2, 65536);
		assert(err != cudaErrorMemoryAllocation);
	}
}

void CudaWrapperB::SetAndBindTexRefSource0(ImageInfoB ii)
{
	texSourceB0.addressMode[0] = texRef.addressMode[0];
	texSourceB0.addressMode[1] = texRef.addressMode[1];
	texSourceB0.filterMode = texRef.filterMode;
	texSourceB0.normalized = texRef.normalized;
	size_t offset;
	cudaError_t err = cudaBindTexture2D(&offset, texSourceB0, ii.d_buffer, cudaCreateChannelDesc(8,0,0,0,cudaChannelFormatKindUnsigned), ii.width, ii.height, ii.pitchInBytes);
	assert(err != cudaErrorInvalidValue);
	assert(err != cudaErrorInvalidDevicePointer);
	assert(err != cudaErrorInvalidTexture);
	assert(err == cudaSuccess);
	assert(offset == 0);
}

void CudaWrapperB::SetAndBindTexRefSource1(ImageInfoB ii)
{
	texSourceB1.addressMode[0] = texRef.addressMode[0];
	texSourceB1.addressMode[1] = texRef.addressMode[1];
	texSourceB1.filterMode = texRef.filterMode;
	texSourceB1.normalized = texRef.normalized;
	size_t offset;
	cudaError_t err = cudaBindTexture2D(&offset, texSourceB1, ii.d_buffer, cudaCreateChannelDesc(8,0,0,0,cudaChannelFormatKindUnsigned), ii.width, ii.height, ii.pitchInBytes);
	assert(offset == 0);
}

void CudaWrapperB::GetAsByteArray(Byte *dest)
{
	cudaMemcpy2D(dest, info.pitchInBytes, info.d_buffer, info.pitchInBytes, info.width * sizeof(Byte), info.height, cudaMemcpyDeviceToHost);
}

void CudaWrapperB::SetFromByteArray(Byte *source)
{
	cudaMemcpy2D(info.d_buffer, info.pitchInBytes, source, info.pitchInBytes, info.width * sizeof(Byte), info.height, cudaMemcpyHostToDevice);
}

void CudaWrapperB::SetPixel(int x, int y, Byte val)
{
	//float* pElement = (float*)((char*)d_buffer + y * m_pitchInBytes) + x;
	Byte* pElement = (Byte*)(info.d_buffer + y * info.pitchInBytes) + x;
	// probably shouldn't do Async since "val" will go out of scope immediately.
	cudaMemcpy2D(pElement, info.pitchInBytes, &val, 1, 1, 1, cudaMemcpyHostToDevice);
}

Byte CudaWrapperB::GetPixel(int x, int y)
{
	//float* pElement = (float*)((char*)d_buffer + y * m_pitchInBytes) + x;
	Byte* pElement = (Byte*)(info.d_buffer + y * info.pitchInBytes) + x;
	Byte result;
	cudaMemcpy2D(&result, info.pitchInBytes, pElement, info.pitchInBytes, info.width*sizeof(Byte), info.height, cudaMemcpyDeviceToHost);
	return result;
}

void CudaWrapperB::Clear()
{
	cudaMemset2D(info.d_buffer, info.pitchInBytes, 0, info.width * sizeof(Byte), info.height);
}

void CudaWrapperB::BasicKernel2Image(CudaWrapperB *source0, CudaWrapperB *source1, int op)
{
	assert(source0 != this);
	assert(source1 != this);
	SetAndBindTexRefSource0(source0->info);
	SetAndBindTexRefSource1(source1->info);
	switch (op)
	{
		case 0:
		AddKernelB<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 1:
		SubKernelB<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 2:
		MulKernelB<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 3:
		DivKernelB<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 4:
		PowKernelB<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		default: assert(0);
	}
	cudaUnbindTexture(texSourceB1);
	cudaUnbindTexture(texSourceB0);
}

void CudaWrapperB::BasicKernel1Const(CudaWrapperB *source0, float val, int op)
{
	assert(source0 != this);
	SetAndBindTexRefSource0(source0->info);
	switch (op)
	{
		case 0:
		AddConstKernelB<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		case 1:
		AddConstKernelB<<< dimGrid, dimBlock >>>(info, source0->info, -val);
		break;
		case 2:
		MulConstKernelB<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		case 3:
		DivConstKernelB<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		case 4:
		PowConstKernelB<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		case 5:
		scaleBicubicKernelTexB<<< dimGrid, dimBlock >>>(info, source0->info, 1.0f/val);
		break;
		case 6:
		texSourceB0.filterMode = cudaFilterModePoint;
		ScaleKernelB<<< dimGrid, dimBlock >>>(info, source0->info, 1.0f/val);
		break;
		case 7:
		ScaleKernelB<<< dimGrid, dimBlock >>>(info, source0->info, 1.0f/val);
		break;
		case 8:
		rotateKernelTexB<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		default: assert(0);
	}
	cudaUnbindTexture(texSourceB0);
}

void CudaWrapperB::ConvertToARGB(int *dest, int destPitchInInts, int w, int h)
{
	dim3 dimBlock2(16, 8, 1);
	dim3 dimGrid2(destPitchInInts / dimBlock2.x + 1, h / dimBlock2.y + 1, 1);	// +1 to round up for odd numbers

	int *d_tempBuffer;
	cudaMalloc(&d_tempBuffer, destPitchInInts*4*h);
	CudaConvertByteToARGB<<< dimGrid2, dimBlock2 >>>(d_tempBuffer, destPitchInInts, h, info);
	cudaMemcpy(dest, d_tempBuffer, destPitchInInts*4*h, cudaMemcpyDeviceToHost);
	cudaFree(d_tempBuffer);
}

void CudaWrapperB::GenRandom(CudaWrapperB *randImage0, CudaWrapperB *randImage1, int randOffset)
{
	SetAndBindTexRefSource0(randImage0->info);
	SetAndBindTexRefSource1(randImage1->info);
	GenRandomKernelTexB<<< dimGrid, dimBlock >>>(1.0f/randImage0->info.width, 1.0f/randImage0->info.height, 1.0f/randImage1->info.width, 1.0f/randImage1->info.height, randOffset, info);
	cudaUnbindTexture(texSourceB1);
	cudaUnbindTexture(texSourceB0);
}

//void CudaWrapperB::GenPerlin(CudaWrapperB *randImage0, CudaWrapperB *randImage1, int randOffset, float scale)
//{
//	SetAndBindTexRefSource0(randImage0->info);
//	SetAndBindTexRefSource1(randImage1->info);
//	GenPerlinKernelTexB<<< dimGrid, dimBlock >>>(randImage0->info, randImage1->info, randOffset, 1.0f/scale, info);
//	cudaUnbindTexture(texSourceB1);
//	cudaUnbindTexture(texSourceB0);
//}

// This will fill out an array of 4 ints with a bounding box. minx,miny,maxx,maxy
void CudaWrapperB::FindExtents(int *boundingBoxResult)
{
	int *d_tempBuffer;
	cudaError_t err = cudaMalloc(&d_tempBuffer, 4*4);
	assert(err != cudaErrorMemoryAllocation);
	boundingBoxResult[0] = INT_MAX;
	boundingBoxResult[1] = INT_MAX;
	boundingBoxResult[2] = INT_MIN;
	boundingBoxResult[3] = INT_MIN;
	cudaMemcpy(d_tempBuffer, boundingBoxResult, 4*4, cudaMemcpyHostToDevice);
	SetAndBindTexRefSource0(info);
	FindExtentsKernelB<<< dimGrid, dimBlock >>>(d_tempBuffer, info);
	cudaUnbindTexture(texSourceB0);

	//int boundingBoxResult[4];
	cudaMemcpy(boundingBoxResult, d_tempBuffer, 4*4, cudaMemcpyDeviceToHost);
	cudaFree(d_tempBuffer);
}

void CudaWrapperB::FindExtentsAndCopy(CudaWrapperB *source0)
{
	int *d_tempBuffer;
	//cudaError_t err = cudaMalloc(&d_tempBuffer, 4*4);
	//assert(err != cudaErrorMemoryAllocation);
	int boundingBoxResult[4];
	boundingBoxResult[0] = INT_MAX;
	boundingBoxResult[1] = INT_MAX;
	boundingBoxResult[2] = INT_MIN;
	boundingBoxResult[3] = INT_MIN;
	cudaError_t err = cudaMemcpy(d_scratchPad2, boundingBoxResult, 4*4, cudaMemcpyHostToDevice);
	assert(err != cudaErrorMemoryAllocation);
	SetAndBindTexRefSource0(source0->info);
	assert(err != cudaErrorMemoryAllocation);
	FindExtentsKernelB<<< source0->dimGrid, source0->dimBlock >>>(d_scratchPad2, source0->info);
	assert(err != cudaErrorMemoryAllocation);
	cudaUnbindTexture(texSourceB0);
	SetAndBindTexRefSource0(source0->info);
	CopyFromScaledRegionKernelB<<< dimGrid, dimBlock >>>(info, source0->info, d_scratchPad2);
	cudaUnbindTexture(texSourceB0);

	//cudaFree(d_tempBuffer);
}

void CudaWrapperB::CopyFromScaledRegion(CudaWrapperB *source0, int *boundingBox)
{
	assert(source0 != this);
	SetAndBindTexRefSource0(source0->info);

	CopyFromScaledRegionKernelB<<< dimGrid, dimBlock >>>(info, source0->info, boundingBox[0], boundingBox[1], boundingBox[2], boundingBox[3]);

	cudaUnbindTexture(texSourceB0);
}




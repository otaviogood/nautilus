// Copyright 2014 Otavio Good
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#pragma once
#ifndef BASIC_CU
#define BASIC_CU

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <builtin_types.h>
#include <device_launch_parameters.h>
#include <device_functions.h>
#include <cufft.h>
#include <assert.h>

#include "cutil_math.h"
#include "CudaFloat.h"

#ifdef _DEBUG
#define ASSERT(x) assert(x)
#else
#define ASSERT(x) assert(x)
//#define ASSERT(x) ;
#endif

texture<float, cudaTextureType2D, cudaReadModeElementType> texSource0;
texture<float, cudaTextureType2D, cudaReadModeElementType> texSource1;
texture<float, cudaTextureType2D, cudaReadModeElementType> texSource2;

__device__ void SetPixelF(ImageInfo ii, int x, int y, float val)
{
	ii.d_buffer[y * ii.pitchInFloats + x] = val;
}
__device__ void SetPixelF(float* const destBuffer, const int destPitchInFloats, const int x, const int y, const float val)
{
	destBuffer[y * destPitchInFloats + x] = val;
}

// 63000 timing.
// 52000-53000 - reduced sin/cos compute.
// 42000 - fast __sincosf, 12.131ms
// 43000 - added if conditions for bounds.
// 31000 - dimblock 16,16
// 24000(laptop)/15000(desktop) - passed in invWidth and invHeight
// 22-23000 - dimblock 16, 8
//__restrict
// templates?
__global__ void rotateKernelTexF(ImageInfo dest, ImageInfo source, float theta)
{
    // calculate normalized texture coordinates
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u = (x+0.5f) * source.invWidth;
    float v = (y+0.5f) * source.invHeight;

    // transform coordinates
    u -= 0.5f;
    v -= 0.5f;
	float sTemp;
	float cTemp;
	__sincosf(theta, &sTemp, &cTemp);
    float tu = u*cTemp - v*sTemp + 0.5f;
    float tv = v*cTemp + u*sTemp + 0.5f;

	//dest.d_buffer[y*dest.pitchInFloats + x] = GetPixelBilinear(source, (tu), (tv));
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, tu, tv));
}

__global__ void CudaConvertFloatToARGB(int* dest, int destPitchInInts, int height, ImageInfo source)
{
    // calculate normalized texture coordinates
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= destPitchInInts) return;
	if (y >= height) return;

    float u = x;// / (float) source.width;
    float v = y;// / (float) source.height;

	float pix = 0;
	if ((u < source.width) && (v < source.height)) pix = source.d_buffer[(int)v * source.pitchInFloats + (int)u];
	int positive = max(min(pix, 255.0f), 0.0f);
	int negative = max(min(-pix, 255.0f), 0.0f);
	//dest[y * destPitchInInts + x] = 0xff000000 | (positive << 8) + (negative << 0) + ((negative<<7) &0xff00);
	dest[y * destPitchInInts + x] = 0xff000000 | (positive << 8) + (negative << 16);
}

__global__ void GenRandomKernelTexF(float invW0, float invH0, float invW1, float invH1, int offset, ImageInfo dest)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

	float u0 = (x+0.5f+offset) * invW0;
    float v0 = (y+0.5f+offset) * invH0;
	float u1 = (x+0.5f+offset) * invW1;
    float v1 = (y+0.5f+offset) * invH1;
	int a = (int)tex2D(texSource0, u0, v0);
	int b = (int)tex2D(texSource1, u1, v1);
	int c = a ^ b;

	SetPixelF(dest, x, y, c >> 8);
}

__device__ float GetPixelPerlinF(ImageInfo info0, ImageInfo info1, float x, float y)
{
	// http://www.lems.brown.edu/vision/vxl_doc/html/core/vil/html/
	float p1x = (int)x;
	float normx = x - p1x;
	float p1y = (int)y;
	float normy = y - p1y;

	// like bilinear interpolation, use separability.
	// the s's are for the x-direction and the t's for the y-direction.
	float s0 = ((2 - normx) * normx - 1) * normx;    // -1
	float s1 = (3 * normx - 5) * normx * normx + 2;    //  0
	float s2 = ((4 - 3 * normx) * normx + 1) * normx;  // +1
	float s3 = (normx - 1) * normx * normx;        // +2

	float t0 = ((2 - normy) * normy - 1) * normy;
	float t1 = (3 * normy - 5) * normy * normy + 2;
	float t2 = ((4 - 3 * normy) * normy + 1) * normy;
	float t3 = (normy - 1) * normy * normy;

	float invWidth0 = info0.invWidth;
	float invHeight0 = info0.invHeight;
	float invWidth02 = invWidth0 * 2;
	float invHeight02 = invHeight0 * 2;
	float invWidth1 = info1.invWidth;
	float invHeight1 = info1.invHeight;
	float invWidth12 = invWidth1 * 2;
	float invHeight12 = invHeight1 * 2;

	float p1x0 = p1x * invWidth0;
	float p1y0 = p1y * invHeight0;
	float p1x1 = p1x * invWidth1;
	float p1y1 = p1y * invHeight1;

	float xi0 =
		((int)tex2D(texSource0, p1x0 - invWidth0, p1y0 - invHeight0) ^ (int)tex2D(texSource1, p1x1 - invWidth1, p1y1 - invHeight1) ) * s0 +
		((int)tex2D(texSource0, p1x0 + 0, p1y0 - invHeight0) ^ (int)tex2D(texSource1, p1x1 + 0, p1y1 - invHeight1)) * s1 +
		((int)tex2D(texSource0, p1x0 + invWidth0, p1y0 - invHeight0) ^ (int)tex2D(texSource1, p1x1 + invWidth1, p1y1 - invHeight1)) * s2 +
		((int)tex2D(texSource0, p1x0 + invWidth02, p1y0 - invHeight0) ^ (int)tex2D(texSource1, p1x1 + invWidth12, p1y1 - invHeight1)) * s3;
	float xi1 =
		((int)tex2D(texSource0, p1x0 - invWidth0, p1y0 + 0) ^ (int)tex2D(texSource1, p1x1 - invWidth1, p1y1 + 0)) * s0 +
		((int)tex2D(texSource0, p1x0 + 0, p1y0 + 0) ^ (int)tex2D(texSource1, p1x1 + 0, p1y1 + 0)) * s1+
		((int)tex2D(texSource0, p1x0 + invWidth0, p1y0 + 0) ^ (int)tex2D(texSource1, p1x1 + invWidth1, p1y1 + 0)) * s2 +
		((int)tex2D(texSource0, p1x0 + invWidth02, p1y0 + 0) ^ (int)tex2D(texSource1, p1x1 + invWidth12, p1y1 + 0)) * s3;
	float xi2 =
		((int)tex2D(texSource0, p1x0 - invWidth0, p1y0 + invHeight0) ^ (int)tex2D(texSource1, p1x1 - invWidth1, p1y1 + invHeight1)) * s0 +
		((int)tex2D(texSource0, p1x0 + 0, p1y0 + invHeight0) ^ (int)tex2D(texSource1, p1x1 + 0, p1y1 + invHeight1)) * s1 +
		((int)tex2D(texSource0, p1x0 + invWidth0, p1y0 + invHeight0) ^ (int)tex2D(texSource1, p1x1 + invWidth1, p1y1 + invHeight1)) * s2 +
		((int)tex2D(texSource0, p1x0 + invWidth02, p1y0 + invHeight0) ^ (int)tex2D(texSource1, p1x1 + invWidth12, p1y1 + invHeight1)) * s3;
	float xi3 =
		((int)tex2D(texSource0, p1x0 - invWidth0, p1y0 + invHeight02) ^ (int)tex2D(texSource1, p1x1 - invWidth1, p1y1 + invHeight12)) * s0 +
		((int)tex2D(texSource0, p1x0 + 0, p1y0 + invHeight02) ^ (int)tex2D(texSource1, p1x1 + 0, p1y1 + invHeight12)) * s1 +
		((int)tex2D(texSource0, p1x0 + invWidth0, p1y0 + invHeight02) ^ (int)tex2D(texSource1, p1x1 + invWidth1, p1y1 + invHeight12)) * s2 +
		((int)tex2D(texSource0, p1x0 + invWidth02, p1y0 + invHeight02) ^ (int)tex2D(texSource1, p1x1 + invWidth12, p1y1 + invHeight12)) * s3;

	float val = (xi0 * t0 + xi1 * t1 + xi2 * t2 + xi3 * t3) * 0.25f;

	return val;
}

__global__ void GenPerlinKernelTexF(ImageInfo rand0, ImageInfo rand1, int offset, float invScale, ImageInfo dest)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

	float u0 = (x+0.5f+offset) * invScale;
    float v0 = (y+0.5f+offset) * invScale;

	SetPixelF(dest, x, y, GetPixelPerlinF(rand0, rand1, u0, v0));
}

__device__ float GetPixelBicubicF(ImageInfo info, float x, float y)
{
	// http://www.lems.brown.edu/vision/vxl_doc/html/core/vil/html/
	float p1x = (int)x;
	float normx = x - p1x;
	float p1y = (int)y;
	float normy = y - p1y;

	// like bilinear interpolation, use separability.
	// the s's are for the x-direction and the t's for the y-direction.
	float s0 = ((2 - normx) * normx - 1) * normx;    // -1
	float s1 = (3 * normx - 5) * normx * normx + 2;    //  0
	float s2 = ((4 - 3 * normx) * normx + 1) * normx;  // +1
	float s3 = (normx - 1) * normx * normx;        // +2

	float t0 = ((2 - normy) * normy - 1) * normy;
	float t1 = (3 * normy - 5) * normy * normy + 2;
	float t2 = ((4 - 3 * normy) * normy + 1) * normy;
	float t3 = (normy - 1) * normy * normy;

	float invWidth = info.invWidth;
	float invHeight = info.invHeight;
	float invWidth2 = invWidth * 2;
	float invHeight2 = invHeight * 2;

	p1x *= invWidth;
	p1y *= invHeight;

	float xi0 = tex2D(texSource0, p1x - invWidth, p1y - invHeight) * s0		+ tex2D(texSource0, p1x + 0, p1y - invHeight) * s1	+ tex2D(texSource0, p1x + invWidth, p1y - invHeight) * s2	+ tex2D(texSource0, p1x + invWidth2, p1y - invHeight) * s3;
	float xi1 = tex2D(texSource0, p1x - invWidth, p1y + 0) * s0				+ tex2D(texSource0, p1x + 0, p1y + 0) * s1			+ tex2D(texSource0, p1x + invWidth, p1y + 0) * s2			+ tex2D(texSource0, p1x + invWidth2, p1y + 0) * s3;
	float xi2 = tex2D(texSource0, p1x - invWidth, p1y + invHeight) * s0		+ tex2D(texSource0, p1x + 0, p1y + invHeight) * s1	+ tex2D(texSource0, p1x + invWidth, p1y + invHeight) * s2	+ tex2D(texSource0, p1x + invWidth2, p1y + invHeight) * s3;
	float xi3 = tex2D(texSource0, p1x - invWidth, p1y + invHeight2) * s0	+ tex2D(texSource0, p1x + 0, p1y + invHeight2) * s1	+ tex2D(texSource0, p1x + invWidth, p1y + invHeight2) * s2	+ tex2D(texSource0, p1x + invWidth2, p1y + invHeight2) * s3;
	//float xi0 = tex2D(texSource0, p1x - 1, p1y - 1) * s0 + tex2D(texSource0, p1x + 0, p1y - 1) * s1 + tex2D(texSource0, p1x + 1, p1y - 1) * s2 + tex2D(texSource0, p1x + 2, p1y - 1) * s3;
	//float xi1 = tex2D(texSource0, p1x - 1, p1y + 0) * s0 + tex2D(texSource0, p1x + 0, p1y + 0) * s1 + tex2D(texSource0, p1x + 1, p1y + 0) * s2 + tex2D(texSource0, p1x + 2, p1y + 0) * s3;
	//float xi2 = tex2D(texSource0, p1x - 1, p1y + 1) * s0 + tex2D(texSource0, p1x + 0, p1y + 1) * s1 + tex2D(texSource0, p1x + 1, p1y + 1) * s2 + tex2D(texSource0, p1x + 2, p1y + 1) * s3;
	//float xi3 = tex2D(texSource0, p1x - 1, p1y + 2) * s0 + tex2D(texSource0, p1x + 0, p1y + 2) * s1 + tex2D(texSource0, p1x + 1, p1y + 2) * s2 + tex2D(texSource0, p1x + 2, p1y + 2) * s3;

	float val = (xi0 * t0 + xi1 * t1 + xi2 * t2 + xi3 * t3) * 0.25f;

	return val;
}

__global__ void scaleBicubicKernelTexF(ImageInfo dest, ImageInfo source, float invScale)
{
    // calculate normalized texture coordinates
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, GetPixelBicubicF(source, (x+0.5f)*invScale, (y+0.5f)*invScale));
	//float u = x * source.invWidth;
	//float v = y * source.invHeight;
	//SetPixel(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u*invScale, v*invScale));
}

__global__ void ScaleKernelF(ImageInfo dest, ImageInfo source, float invScale)
{
    // calculate normalized texture coordinates
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

	float u = (x+0.5f) * source.invWidth;
	float v = (y+0.5f) * source.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u*invScale, v*invScale));
}

__global__ void AddKernelF(ImageInfo dest, ImageInfo source0, ImageInfo source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u0, v0) + tex2D(texSource1, u1, v1));
}

__global__ void SubKernelF(ImageInfo dest, ImageInfo source0, ImageInfo source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u0, v0) - tex2D(texSource1, u1, v1));
}

__global__ void MulKernelF(ImageInfo dest, ImageInfo source0, ImageInfo source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u0, v0) * tex2D(texSource1, u1, v1));
}

__global__ void DivKernelF(ImageInfo dest, ImageInfo source0, ImageInfo source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u0, v0) / tex2D(texSource1, u1, v1));
}

__global__ void PowKernelF(ImageInfo dest, ImageInfo source0, ImageInfo source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, __powf(tex2D(texSource0, u0, v0), tex2D(texSource1, u1, v1)));
}

// Use Add for Subtract.
__global__ void AddConstKernelF(ImageInfo dest, ImageInfo source0, float val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u0, v0) + val);
}

__global__ void MulConstKernelF(ImageInfo dest, ImageInfo source0, float val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u0, v0) * val);
}

__global__ void DivConstKernelF(ImageInfo dest, ImageInfo source0, float val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u0, v0) / val);
}

__global__ void PowConstKernelF(ImageInfo dest, ImageInfo source0, float val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, __powf(tex2D(texSource0, u0, v0), val));
}

__global__ void FindExtentsKernelF(int* dest, ImageInfo source0)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= source0.width) return;
	if (y >= source0.height) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	float pix = tex2D(texSource0, u0, v0);
	if (pix > 127)// != 0)
	//if (pix != 0)
	{
		atomicMin(&(dest[0]), x);
		atomicMin(&(dest[1]), y);
		atomicMax(&(dest[2]), x);
		atomicMax(&(dest[3]), y);
	}
}

__global__ void FindExtentsKernelBrokenF(int* dest, ImageInfo source0)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= source0.width) return;
	if (y >= source0.height) return;

	__shared__ int tempBounds[4];

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
	float pix = tex2D(texSource0, u0, v0);
	if (pix != 0)
	{
		atomicMin(&(tempBounds[0]), x);
		atomicMin(&(tempBounds[1]), y);
		atomicMax(&(tempBounds[2]), x);
		atomicMax(&(tempBounds[3]), y);
	}

	__syncthreads();
	//for (int e = 0; e < BLOCK_SIZE; ++e)
}

__global__ void TerrainRenderKernel(ImageInfo dest, ImageInfo source0, float val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

	float u0 = (x+0.5f) * dest.invWidth;
    //float v0 = (y+0.5f) * dest.invHeight;

	float waterLevel = 500.0f;
	float move = val * 0.00125f;
	float tex = max(waterLevel,tex2D(texSource0, move, move));
	float cameraHeight = tex/8192 + 0.0075f;//0.06f;
	float cameraScale = 0.05f;
	float3 cameraPos =			make_float3(0.0f+ move, 0.0f+ move, cameraHeight);
	float3 cameraRightBottom =	make_float3(cameraScale+move, 0.0f+ move, cameraHeight-cameraScale);
	float3 cameraLeftBottom =	make_float3(0.0f+ move, cameraScale+move, cameraHeight-cameraScale);
	float3 cameraRightTop =		make_float3(cameraScale+move, 0.0f+ move, cameraHeight+cameraScale);
	float3 cameraLeftTop =		make_float3(0.0f+ move, cameraScale+move, cameraHeight+cameraScale);
	float3 lerpBackTop = lerp(cameraLeftTop, cameraRightTop, u0);
	float3 lerpBackBottom = lerp(cameraLeftBottom, cameraRightBottom, u0);

	float lastGoodHeight = dest.height-1;
	float inc = 0.5f;
	for (float count = -300; count < dest.height; count+=inc)
	{
		inc = 0.25f;
		float3 lerpHeight = lerp(lerpBackBottom, lerpBackTop, (float)count/dest.height);
		float dotCam = dot(cameraPos, make_float3(0,0,1));
		float dotFar = dot(lerpHeight, make_float3(0,0,1));
		float ratio = dotCam / (dotCam-dotFar);
		if ((ratio > 0.0f) && (ratio < 120.0f))
		{
			//float heightScale = (cameraRightBottom.z - cameraRightTop.z)
			float3 pos = lerp(cameraPos, lerpHeight, ratio);
			float tex = max(waterLevel,tex2D(texSource0, pos.x, pos.y));
			float tex2 = max(waterLevel,tex2D(texSource0, pos.x, pos.y + source0.invHeight));
			float2 normal = make_float2(tex-tex2, 4.0f);
			float color = saturate((tex2 - tex)*0.05f)*2+0.25f;
			if (tex == waterLevel) color = -0.4;
			//color = max(0.0f, normal.x)*0.3f;
			float scaledTex = tex/(length(pos-cameraPos)*20+1);
			float yPos = ((dest.height-1)-count) - scaledTex - 100;
			if ((yPos < 0) || (yPos >= lastGoodHeight)) continue;
		//float2 lerpPos = lerp(cameraPos, lerpBack, (float)count/dest.height);
			for (int yCount = yPos; yCount < lastGoodHeight; yCount++)
			//float yCount = yPos;
				SetPixelF(dest.d_buffer, dest.pitchInFloats, x, yCount, color*128);
			lastGoodHeight = yPos - 1;
		}
		//else for (int yCount = yPos; yCount < lastGoodHeight; yCount++) SetPixel(dest.d_buffer, dest.pitchInFloats, x, yCount, -1);
	}
//	for (int count = lastGoodHeight; count >= 0; count--) SetPixel(dest.d_buffer, dest.pitchInFloats, x, count, -1);
}

__global__ void CopyFromScaledRegionKernelF(ImageInfo dest, ImageInfo source0, int minx, int miny, int maxx, int maxy)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * dest.invWidth;
    float v0 = (y+0.5f) * dest.invHeight;
	u0 = minx * source0.invWidth + (maxx-minx)*source0.invWidth *u0;
	v0 = miny * source0.invHeight+ (maxy-miny)*source0.invHeight*v0;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, tex2D(texSource0, u0, v0));
}

__global__ void CopyFromScaledRegionKernelF(ImageInfo dest, ImageInfo source0, int *bounds)// int minx, int miny, int maxx, int maxy)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;
	float minx = bounds[0];
	float miny = bounds[1];
	float maxx = bounds[2];
	float maxy = bounds[3];

    float u0 = (x+0.5f) * dest.invWidth;
    float v0 = (y+0.5f) * dest.invHeight;
	u0 = minx * source0.invWidth + (maxx-minx)*source0.invWidth *u0;
	v0 = miny * source0.invHeight+ (maxy-miny)*source0.invHeight*v0;
	//float pix = ((tex2D(texSource0, u0, v0)-128) * 1.1f) + 128;
	//pix = clamp(pix, 0.0f, 255.0f);
	float pix = tex2D(texSource0, u0, v0);
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, pix);
}

__global__ void DiffKernelF(ImageInfo source0, ImageInfo source1, int *result)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= source0.width) return;
	if (y >= source0.height) return;

	int a = source0.d_buffer[y * source0.pitchInFloats + x];
	int b = source1.d_buffer[y * source1.pitchInFloats + x];
	int total = (a-b)*(a-b);

	atomicAdd(&(result[0]), total);
}

__global__ void DiffKernel2F(ImageInfo source0, float *other, int *result)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= source0.width) return;
	if (y >= source0.height) return;

	int a = source0.d_buffer[y * source0.pitchInFloats + x];
	int b = other[y * source0.pitchInFloats + x];
	int total = (a-b)*(a-b);

	atomicAdd(&(result[0]), total);
}

__global__ void DiffKernel3F(ImageInfo source0, float **other, int *result, int numElements)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	const int size = 2;
	x *= size;
	y *= size;
	if (x >= source0.width) return;
	if (y >= source0.height) return;

	for (int count = 0; count < numElements; count++)
	{
		float *otherBmp = other[count];
		int a;
		int b;
		int total = 0;
		for (int yCount = 0; yCount < size; yCount++)
		{
			for (int xCount = 0; xCount < size; xCount++)
			{
				a = source0.d_buffer[(y+yCount) * source0.pitchInFloats + x + xCount];
				b = otherBmp[(y+yCount) * source0.pitchInFloats + x + xCount];
				total += (a-b)*(a-b);
			}
		}
		atomicAdd(&(result[count]), total);
	}

}

__global__ void GenerateGaussianKernel(ImageInfo dest, float xPos, float yPos, float radius, float magnitude)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

	float2 center = make_float2(xPos, yPos);
	float standardDeviation = 1.0f;

	float2 uv = make_float2((float)x - xPos, (float)y - yPos);
	float r = (length(uv) * 10.0f) / radius;     // arbitrary scalar I made up to make the blur the right size
	float E = 2.71828f;
	float main = (rsqrtf(2.0f * 3.1415926f * standardDeviation * standardDeviation)) * E;
	float exp = -(r * r) / (2.0f * standardDeviation * standardDeviation);
	float intensity = powf(main, exp);
	//float old = GetPixelSafe(x + (int)xPos, y + (int)yPos);
	//SetPixelSafe(x + (int)xPos, y + (int)yPos, intensity * magnitude + old);


    //float u0 = (x+0.5f) * source0.invWidth;
    //float v0 = (y+0.5f) * source0.invHeight;
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, intensity * magnitude);// + old);
}

__global__ void WaveGaussianKernel(ImageInfo destHeight, ImageInfo destVel, ImageInfo heightBuffer, ImageInfo wallBuffer, ImageInfo velBuffer)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((x >= destHeight.width) || (y >= destHeight.height)) return;
    float u0 = (x+0.5f) * destHeight.invWidth;
    float v0 = (y+0.5f) * destHeight.invHeight;
	float iw = destHeight.invWidth;
	float ih = destHeight.invHeight;

	float height = tex2D(texSource0, u0, v0);// heightBuffer.GetPixel(x, y);
	float h00 = tex2D(texSource0, u0 - iw, v0 - ih);
	float h10 = tex2D(texSource0, u0 + 0,  v0 - ih);
	float h20 = tex2D(texSource0, u0 + iw, v0 - ih);
	float h01 = tex2D(texSource0, u0 - iw, v0);
	float h21 = tex2D(texSource0, u0 + iw, v0);
	float h02 = tex2D(texSource0, u0 - iw, v0 + ih);
	float h12 = tex2D(texSource0, u0 + 0,  v0 + ih);
	float h22 = tex2D(texSource0, u0 + iw, v0 + ih);

	float w00 = tex2D(texSource1, u0 - iw, v0 - ih);// & 1;
	float w10 = tex2D(texSource1, u0 + 0,  v0 - ih);// & 1;
	float w20 = tex2D(texSource1, u0 + iw, v0 - ih);// & 1;
	float w01 = tex2D(texSource1, u0 - iw, v0);// & 1;
	float w21 = tex2D(texSource1, u0 + iw, v0);// & 1;
	float w02 = tex2D(texSource1, u0 - iw, v0 + ih);// & 1;
	float w12 = tex2D(texSource1, u0 + 0,  v0 + ih);// & 1;
	float w22 = tex2D(texSource1, u0 + iw, v0 + ih);// & 1;

	float speed = tex2D(texSource2, u0, v0);

	const float corner = 0.066585f;
	const float side = 0.18341f;
	float avg = (h10 + h01 + h21 + h12) * side;
	avg += (h00 + h20 + h02 + h22) * corner;
	float wallCount = w00 + w10 + w20 + w01 + w21 + w02 + w12 + w22;
	if (wallCount != 0)
	{
		w00 = saturate((255-w00)*0.00390625f) * corner;
		w20 = saturate((255-w20)*0.00390625f) * corner;
		w02 = saturate((255-w02)*0.00390625f) * corner;
		w22 = saturate((255-w22)*0.00390625f) * corner;
		w10 = saturate((255-w10)*0.00390625f) * side;
		w01 = saturate((255-w01)*0.00390625f) * side;
		w21 = saturate((255-w21)*0.00390625f) * side;
		w12 = saturate((255-w12)*0.00390625f) * side;
		//w00 *= corner;
		//w20 *= corner;
		//w02 *= corner;
		//w22 *= corner;
		//w10 *= side;
		//w01 *= side;
		//w21 *= side;
		//w12 *= side;
		avg = (h00 * w00 + h01 * w01 + h02 * w02 + h10 * w10 + h12 * w12 + h20 * w20 + h21 * w21 + h22 * w22);// / (float)wallCount;
	}
	//uint meWall = wallBuffer.GetPixel(x, y) & 1;
	//if (meWall != 0)
	//{
	//    avg = height;
	//    height = 0;
	//    speed = 0;
	//}
	//if ((x<=10) || (y<=10) || (x==destHeight.width-1) || (y==destHeight.height-1))
	//{
	//    avg = height;
	//    height = 0;
	//    speed = 0;
	//}
	float force = (avg - height) * 0.9f;
	speed += force;
	speed *= 0.9999f;
	//if (wallCount != 0) speed *= 0.0f;
	height += speed;
	SetPixelF(destHeight.d_buffer, destHeight.pitchInFloats, x, y, height);
	SetPixelF(destVel.d_buffer, destVel.pitchInFloats, x, y, speed);
	//heightBufferOut.SetPixel(x, y, height);
	//velBufferOut.SetPixel(x, y, speed);
}

//__global__ void ReductionAddKernelF(ImageInfo dest, ImageInfo source0)
//{
//	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
//	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
//	if ((x >= source0.width) || (y >= source0.height)) return;
//
//    float u0 = (x+0.5f) * source0.invWidth;
//    float v0 = (y+0.5f) * source0.invHeight;
//
//	float a = tex2D(texSource0, u0, v0);
//	atomicAdd(&(dest.d_buffer[0]), a);
//}

__global__ void ReductionAddIntegerKernelF(int *result, ImageInfo source0)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((x >= source0.width) || (y >= source0.height)) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;

	int a = tex2D(texSource0, u0, v0) * 65536.0f;
	atomicAdd(&(result[0]), a);
}

__global__ void SeperableBlurXKernelF(ImageInfo dest, ImageInfo source0, ImageInfo normalizedKernel)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((x >= dest.width) || (y >= dest.height)) return;

	float u0 = (x+0.5f) * source0.invWidth;
	float v0 = (y+0.5f) * source0.invHeight;

	int kOffset = (normalizedKernel.width - 1) / 2;
	float total = 0;
	float kTotal = 0.0f;
	for (int kX = -kOffset; kX <= kOffset; kX++)
	{
		float kVal = tex2D(texSource1, (kX + kOffset + 0.5f) * normalizedKernel.invWidth, (kOffset + 0.5f) * normalizedKernel.invHeight);
		kTotal += kVal;
		float pix = tex2D(texSource0, u0 + kX*source0.invWidth, v0) * kVal;
		total += pix;
	}
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, total / kTotal);
}

__global__ void SeperableBlurYKernelF(ImageInfo dest, ImageInfo source0, ImageInfo normalizedKernel)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((x >= dest.width) || (y >= dest.height)) return;

	float u0 = (x+0.5f) * source0.invWidth;
	float v0 = (y+0.5f) * source0.invHeight;

	int kOffset = (normalizedKernel.height - 1) / 2;
	float total = 0;
	float kTotal = 0.0f;
	for (int kY = -kOffset; kY <= kOffset; kY++)
	{
		float kVal = tex2D(texSource1, (kOffset + 0.5f) * normalizedKernel.invWidth, (kY + kOffset + 0.5f) * normalizedKernel.invHeight);
		kTotal += kVal;
		float pix = tex2D(texSource0, u0, v0 + kY*source0.invHeight) * kVal;
		total += pix;
	}
	float pix = total / kTotal;
	//pix = ((pix-128) * 0.9f) + 128;
	//pix = clamp(pix, 0.0f, 255.0f);
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, pix);
}

__global__ void ExtractPhaseAndMagnitudeKernel(ImageInfo source0, ImageInfo destPhase, ImageInfo destMagnitude)
{
	unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
	unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if ((x >= destPhase.width) || (y >= destPhase.height)) return;

	float u0 = ((x*2)+0.5f) * source0.invWidth;
	float u1 = ((x*2+1)+0.5f) * source0.invWidth;
	float v0 = (y+0.5f) * source0.invHeight;

	float pixX = tex2D(texSource0, u0, v0);
	float pixY = tex2D(texSource0, u1, v0);
	float mag = sqrtf(pixX * pixX + pixY * pixY);
	float phase = atan2f(pixX, pixY);
	SetPixelF(destPhase.d_buffer, destPhase.pitchInFloats, x, y, phase);
	SetPixelF(destMagnitude.d_buffer, destMagnitude.pitchInFloats, x, y, mag);
}

__global__ void ComplexConjugateKernel(ImageInfo dest, ImageInfo source0)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;

    float u0 = (x+0.5f) * dest.invWidth;
    float v0 = (y+0.5f) * dest.invHeight;
	float pix = tex2D(texSource0, u0, v0);
	if ((x & 1) == 1)
	{
		SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, -pix);
	}
	else
	{
		SetPixelF(dest.d_buffer, dest.pitchInFloats, x, y, pix);
	}
}

__global__ void ComplexMultiplyKernel(ImageInfo dest, ImageInfo source0, ImageInfo source1)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= dest.width) return;
	if (y >= dest.height) return;
	if ((x&1) == 1) return;

    float u0 = (x+0.5f) * source0.invWidth;
    float v0 = (y+0.5f) * source0.invHeight;
    float u1 = (x+0.5f) * source1.invWidth;
    float v1 = (y+0.5f) * source1.invHeight;

	float pa = tex2D(texSource0, u0, v0);// (float)result2dA[x][y].Real;
	float pb = tex2D(texSource0, u0 + source0.invWidth, v0);// (float)result2dA[x][y].Imaginary;
	float pc = tex2D(texSource1, u1, v1);// (float)result2dB[x][y].Real;
	float pd = tex2D(texSource1, u1 + source1.invWidth, v1);//-(float)result2dB[x][y].Imaginary;

	float resReal = pa * pc - pb * pd;
	float resImag = pb * pc + pa * pd;
	float invLen = rsqrtf(resReal * resReal + resImag * resImag);

	//if (len > 0) result2dA[x][y] = new Complex(resReal / len, resImag / len);

	SetPixelF(dest.d_buffer, dest.pitchInFloats, x    , y, resReal * invLen);
	SetPixelF(dest.d_buffer, dest.pitchInFloats, x + 1, y, resImag * invLen);
}

__global__ void FillRectangleKernel(ImageInfo dest, int x0, int y0, int width, int height, float val)
{
    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
    unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	if (x >= width) return;
	if (y >= height) return;

	SetPixelF(dest.d_buffer, dest.pitchInFloats, x0 + x, y0 + y, val);
}

__global__ void FindBrightestPointKernelF(int* dest, ImageInfo source0)
{
 //   unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
 //   unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;
	//if (x >= source0.width) return;
	//if (y >= source0.height) return;

 //   float u0 = (x+0.5f) * source0.invWidth;
 //   float v0 = (y+0.5f) * source0.invHeight;
	//float pix = tex2D(texSource0, u0, v0);
	//atomicMax(&(dest[0]), x);
	//atomicMax(&(dest[1]), y);
}



// ------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------------------

CudaWrapperF::CudaWrapperF(int width, int height)
{
	Init(width, height);
}

CudaWrapperF::~CudaWrapperF()
{
	if (info.d_buffer) cudaFree(info.d_buffer);
}

static int *d_scratchPad = NULL;

void CudaWrapperF::Init(int width, int height)
{
	info.width = width;
	info.height = height;
	info.invWidth = 1.0f / width;
	info.invHeight = 1.0f / height;
	size_t pitchInBytes;
	cudaError_t err = cudaMallocPitch((void **)&info.d_buffer, &pitchInBytes, width * sizeof(float), height);
	assert(err != cudaErrorMemoryAllocation);
	info.pitchInFloats = pitchInBytes/4;
	texRef.addressMode[0] = cudaAddressModeWrap;
	texRef.addressMode[1] = cudaAddressModeWrap;
	texRef.filterMode = cudaFilterModeLinear;
	texRef.normalized = true;
	dimBlock = dim3(16, 8, 1);
	dimGrid = dim3(info.width / dimBlock.x + (((info.width % dimBlock.x)!=0)?1:0), info.height / dimBlock.y + (((info.height % dimBlock.y)!=0)?1:0), 1);
	if (!d_scratchPad)
	{
		cudaError_t err = cudaMalloc(&d_scratchPad, 65536*4);
		assert(err != cudaErrorMemoryAllocation);
	}
}

void CudaWrapperF::SetAndBindTexRefSource0()
{
	texSource0.addressMode[0] = texRef.addressMode[0];
	texSource0.addressMode[1] = texRef.addressMode[1];
	texSource0.filterMode = texRef.filterMode;
	texSource0.normalized = texRef.normalized;
	size_t offset;
	cudaError_t err = cudaBindTexture2D(&offset, texSource0, info.d_buffer, cudaCreateChannelDesc<float>(), info.width, info.height, info.pitchInFloats*4);
	ASSERT(err != cudaErrorInvalidValue);
	ASSERT(err != cudaErrorInvalidDevicePointer);
	ASSERT(err != cudaErrorInvalidTexture);
	ASSERT(err == cudaSuccess);
	ASSERT(offset == 0);
}

void CudaWrapperF::SetAndBindTexRefSource1()
{
	texSource1.addressMode[0] = texRef.addressMode[0];
	texSource1.addressMode[1] = texRef.addressMode[1];
	texSource1.filterMode = texRef.filterMode;
	texSource1.normalized = texRef.normalized;
	size_t offset;
	cudaError_t err = cudaBindTexture2D(&offset, texSource1, info.d_buffer, cudaCreateChannelDesc<float>(), info.width, info.height, info.pitchInFloats*4);
	ASSERT(offset == 0);
}

void CudaWrapperF::SetAndBindTexRefSource2()
{
	texSource2.addressMode[0] = texRef.addressMode[0];
	texSource2.addressMode[1] = texRef.addressMode[1];
	texSource2.filterMode = texRef.filterMode;
	texSource2.normalized = texRef.normalized;
	size_t offset;
	cudaError_t err = cudaBindTexture2D(&offset, texSource2, info.d_buffer, cudaCreateChannelDesc<float>(), info.width, info.height, info.pitchInFloats*4);
	ASSERT(offset == 0);
}

void CudaWrapperF::GetAsFloatArray(float *dest)
{
	cudaMemcpy2D(dest, info.pitchInFloats*4, info.d_buffer, info.pitchInFloats*4, info.width * sizeof(float), info.height, cudaMemcpyDeviceToHost);
}

void CudaWrapperF::SetFromFloatArray(float *source)
{
	cudaMemcpy2D(info.d_buffer, info.pitchInFloats*4, source, info.pitchInFloats*4, info.width * sizeof(float), info.height, cudaMemcpyHostToDevice);
}

void CudaWrapperF::SetFromFloatArrayStride(float *source)
{
	cudaMemcpy2D(info.d_buffer, info.pitchInFloats*4, source, info.width*4, info.width * sizeof(float), info.height, cudaMemcpyHostToDevice);
}

void CudaWrapperF::SetPixel(int x, int y, float val)
{
	//float* pElement = (float*)((char*)d_buffer + y * m_pitchInBytes) + x;
	float* pElement = (float*)(info.d_buffer + y * info.pitchInFloats) + x;
	// probably shouldn't do Async since "val" will go out of scope immediately.
	cudaMemcpy2D(pElement, info.pitchInFloats*4, &val, 4, 4, 1, cudaMemcpyHostToDevice);
}

float CudaWrapperF::GetPixel(int x, int y)
{
	//float* pElement = (float*)((char*)d_buffer + y * m_pitchInBytes) + x;
	float* pElement = (float*)(info.d_buffer + y * info.pitchInFloats) + x;
	float result;
	cudaMemcpy(&result, pElement, 4,cudaMemcpyDeviceToHost);
	return result;
}

void CudaWrapperF::Clear()
{
	cudaMemset2D(info.d_buffer, info.pitchInFloats*4, 0, info.width * sizeof(float), info.height);
}

void CudaWrapperF::BasicKernel2Image(CudaWrapperF *source0, CudaWrapperF *source1, int op)
{
	ASSERT(source0 != this);
	ASSERT(source1 != this);
	source0->SetAndBindTexRefSource0();
	source1->SetAndBindTexRefSource1();
	switch (op)
	{
		case 0:
		AddKernelF<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 1:
		SubKernelF<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 2:
		MulKernelF<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 3:
		DivKernelF<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 4:
		PowKernelF<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 5:
			ASSERT(source1->info.width == source1->info.height);	// has to be square
			ASSERT((source1->info.width & 1) == 1);	// has to be odd so there's a center pixel.
			SeperableBlurXKernelF<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		case 6:
			ComplexMultiplyKernel<<< dimGrid, dimBlock >>>(info, source0->info, source1->info);
		break;
		default: ASSERT(0);
	}
	cudaUnbindTexture(texSource1);
	cudaUnbindTexture(texSource0);
}

void CudaWrapperF::BasicKernel1Const(CudaWrapperF *source0, float val, int op)
{
	ASSERT(source0 != this);
	source0->SetAndBindTexRefSource0();
	switch (op)
	{
		case 0:
		AddConstKernelF<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		case 1:
		AddConstKernelF<<< dimGrid, dimBlock >>>(info, source0->info, -val);
		break;
		case 2:
		MulConstKernelF<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		case 3:
		DivConstKernelF<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		case 4:
		PowConstKernelF<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		case 5:
		scaleBicubicKernelTexF<<< dimGrid, dimBlock >>>(info, source0->info, 1.0f/val);
		break;
		case 6:
		texSource0.filterMode = cudaFilterModePoint;
		ScaleKernelF<<< dimGrid, dimBlock >>>(info, source0->info, 1.0f/val);
		break;
		case 7:
		ScaleKernelF<<< dimGrid, dimBlock >>>(info, source0->info, 1.0f/val);
		break;
		case 8:
		rotateKernelTexF<<< dimGrid, dimBlock >>>(info, source0->info, val);
		break;
		case 9:
		{
			dim3 dimBlock2 = dim3(128, 1, 1);
			dim3 dimGrid2 = dim3(info.width / dimBlock2.x + (((info.width % dimBlock2.x)!=0)?1:0), 1, 1);
			TerrainRenderKernel<<< dimGrid2, dimBlock2 >>>(info, source0->info, val);
		}
		break;
		default: ASSERT(0);
	}
	cudaUnbindTexture(texSource0);
}

void CudaWrapperF::ConvertToARGB(int *dest, int destPitchInInts, int w, int h)
{
	dim3 dimBlock2(16, 8, 1);
	dim3 dimGrid2(destPitchInInts / dimBlock2.x + 1, h / dimBlock2.y + 1, 1);	// +1 to round up for odd numbers

	int *d_tempBuffer;
	cudaMalloc(&d_tempBuffer, destPitchInInts*4*h);
	CudaConvertFloatToARGB<<< dimGrid2, dimBlock2 >>>(d_tempBuffer, destPitchInInts, h, info);
	cudaMemcpy(dest, d_tempBuffer, destPitchInInts*4*h, cudaMemcpyDeviceToHost);
	cudaFree(d_tempBuffer);
}

void CudaWrapperF::GenRandom(CudaWrapperF *randImage0, CudaWrapperF *randImage1, int randOffset)
{
	randImage0->SetAndBindTexRefSource0();
	randImage1->SetAndBindTexRefSource1();
	GenRandomKernelTexF<<< dimGrid, dimBlock >>>(1.0f/randImage0->info.width, 1.0f/randImage0->info.height, 1.0f/randImage1->info.width, 1.0f/randImage1->info.height, randOffset, info);
	cudaUnbindTexture(texSource1);
	cudaUnbindTexture(texSource0);
}

void CudaWrapperF::GenPerlin(CudaWrapperF *randImage0, CudaWrapperF *randImage1, int randOffset, float scale)
{
	randImage0->SetAndBindTexRefSource0();
	randImage1->SetAndBindTexRefSource1();
	GenPerlinKernelTexF<<< dimGrid, dimBlock >>>(randImage0->info, randImage1->info, randOffset, 1.0f/scale, info);
	cudaUnbindTexture(texSource1);
	cudaUnbindTexture(texSource0);
}

// This will fill out an array of 4 ints with a bounding box. minx,miny,maxx,maxy
void CudaWrapperF::FindExtents(int *boundingBoxResult)
{
	int *d_tempBuffer;
	cudaError_t err = cudaMalloc(&d_tempBuffer, 4*4);
	ASSERT(err != cudaErrorMemoryAllocation);
	boundingBoxResult[0] = INT_MAX;
	boundingBoxResult[1] = INT_MAX;
	boundingBoxResult[2] = INT_MIN;
	boundingBoxResult[3] = INT_MIN;
	cudaMemcpy(d_tempBuffer, boundingBoxResult, 4*4, cudaMemcpyHostToDevice);
	SetAndBindTexRefSource0();
	FindExtentsKernelF<<< dimGrid, dimBlock >>>(d_tempBuffer, info);
	cudaUnbindTexture(texSource0);

	//int boundingBoxResult[4];
	cudaMemcpy(boundingBoxResult, d_tempBuffer, 4*4, cudaMemcpyDeviceToHost);
	cudaFree(d_tempBuffer);
}

void CudaWrapperF::FindExtentsAndCopy(CudaWrapperF *source0)
{
	//int *d_tempBuffer;
	//cudaError_t err = cudaMalloc(&d_tempBuffer, 4*4);
	//ASSERT(err != cudaErrorMemoryAllocation);
	int boundingBoxResult[4];
	boundingBoxResult[0] = INT_MAX;
	boundingBoxResult[1] = INT_MAX;
	boundingBoxResult[2] = INT_MIN;
	boundingBoxResult[3] = INT_MIN;
	cudaMemcpy(d_scratchPad, boundingBoxResult, 4*4, cudaMemcpyHostToDevice);
	source0->SetAndBindTexRefSource0();
	FindExtentsKernelF<<< source0->dimGrid, source0->dimBlock >>>(d_scratchPad, source0->info);
	cudaUnbindTexture(texSource0);
	source0->SetAndBindTexRefSource0();
	CopyFromScaledRegionKernelF<<< dimGrid, dimBlock >>>(info, source0->info, d_scratchPad);
	cudaUnbindTexture(texSource0);

	//cudaFree(d_tempBuffer);
}

void CudaWrapperF::CopyFromScaledRegion(CudaWrapperF *source0, int *boundingBox)
{
	ASSERT(source0 != this);
	source0->SetAndBindTexRefSource0();

	CopyFromScaledRegionKernelF<<< dimGrid, dimBlock >>>(info, source0->info, boundingBox[0], boundingBox[1], boundingBox[2], boundingBox[3]);

	cudaUnbindTexture(texSource0);
}

int CudaWrapperF::Diff(CudaWrapperF *source0)
{
	cudaMemset(d_scratchPad, 0, 4);
	DiffKernelF<<< dimGrid, dimBlock >>>(info, source0->info, d_scratchPad);
	int result;
	cudaMemcpy(&result, d_scratchPad, 4, cudaMemcpyDeviceToHost);
	return result;
}

void CudaWrapperF::DiffMultiple(float **allRefPointers, int numElements, int *results)
{
	float **d_tempBuffer;
	cudaError_t err = cudaMalloc(&d_tempBuffer, sizeof(float*)*numElements);
	ASSERT(err == cudaSuccess);
	cudaMemcpy(d_tempBuffer, allRefPointers, sizeof(float*)*numElements, cudaMemcpyHostToDevice);

	ASSERT(numElements < 65536);	// don't overflow the scratchpad.
	cudaMemset(d_scratchPad, 0, 4 * numElements);
	bool isBigPowerOfTwo = ((info.width & (info.width - 1)) == 0) && ((info.height & (info.height - 1)) == 0) && (info.width >= 32);
	if (isBigPowerOfTwo)	// Run the fast version unless I'm trying out some weird resolutions.
	{
		int blockSize = 16;	// 16 is optimal.
		const int subSize = 2;
		dim3 dg2 = dim3((info.width/blockSize)/subSize, (info.height/blockSize)/subSize, 1);
		dim3 db2 = dim3(blockSize, blockSize, 1);
		DiffKernel3F<<< dg2, db2 >>>(info, d_tempBuffer, d_scratchPad, numElements);
	}
	else
	{
		int blockSize = 4;	// 16 is optimal.
		const int subSize = 1;
		dim3 dg2 = dim3((info.width/blockSize)/subSize, (info.height/blockSize)/subSize, 1);
		dim3 db2 = dim3(blockSize, blockSize, 1);
		for (int count = 0; count < numElements; count++)
		{
			DiffKernel2F<<< dimGrid, dimBlock >>>(info, allRefPointers[count], d_scratchPad + count);
		}
	}
	//int blockSize = 8;	// 16 is optimal.
	////ASSERT((info.width & (info.width - 1)) == 0);	// must be power of 2 or my reduction algorithm won't work. :(
	////ASSERT((info.height & (info.height - 1)) == 0);
	//const int subSize = 1;
	//dim3 dg2 = dim3((info.width/blockSize)/subSize, (info.height/blockSize)/subSize, 1);
	//dim3 db2 = dim3(blockSize, blockSize, 1);
	//for (int count = 0; count < numElements; count++)
	////int count = 0;
	//{
	//	DiffKernel2F<<< dimGrid, dimBlock >>>(info, allRefPointers[count], d_scratchPad + count);
	//	//DiffKernel3F<<< dg2, db2 >>>(info, d_tempBuffer, d_scratchPad + count, numElements);
	//}
	cudaMemcpy(results, d_scratchPad, 4 * numElements, cudaMemcpyDeviceToHost);
	cudaFree(d_tempBuffer);
}

void CudaWrapperF::GenGaussian(float xPos, float yPos, float radius, float magnitude)
{
	GenerateGaussianKernel<<<dimGrid, dimBlock>>>(info, xPos, yPos, radius, magnitude);
}

void CudaWrapperF::WaveGaussian(CudaWrapperF *destVel, CudaWrapperF *heightBuffer, CudaWrapperF *wallBuffer, CudaWrapperF *velBuffer)
{
	heightBuffer->SetAndBindTexRefSource0();
	wallBuffer->SetAndBindTexRefSource1();
	velBuffer->SetAndBindTexRefSource2();
	WaveGaussianKernel<<<dimGrid, dimBlock>>>(info, destVel->info, heightBuffer->info, wallBuffer->info, velBuffer->info);
	cudaUnbindTexture(texSource2);
	cudaUnbindTexture(texSource1);
	cudaUnbindTexture(texSource0);
}

CudaWrapperF *CudaWrapperF::GenNormalizedGaussianKernel(float radius)
{
	int iRadius = (int)radius;
	CudaWrapperF tempKernel(iRadius * 2 + 1, iRadius * 2 + 1);
	GenerateGaussianKernel<<<tempKernel.dimGrid, tempKernel.dimBlock>>>(tempKernel.info, radius, radius, radius + 0.5f, 255.0f);
	CudaWrapperF tempTotal(1, 1);
	cudaMemset(d_scratchPad, 0, 4);

	tempKernel.SetAndBindTexRefSource0();
	ReductionAddIntegerKernelF<<<tempKernel.dimGrid, tempKernel.dimBlock>>>(d_scratchPad, tempKernel.info);
	cudaUnbindTexture(texSource0);
	int result;
	cudaMemcpy(&result, d_scratchPad, 4, cudaMemcpyDeviceToHost);

	//tempTotal.BasicKernel1(&tempKernel, 0);	// sum all pixels
	CudaWrapperF *normalizedKernel = new CudaWrapperF(iRadius * 2 + 1, iRadius * 2 + 1);
	normalizedKernel->BasicKernel1Const(&tempKernel, (float)result/65536.0f, 3);	// divide to normalize
	return normalizedKernel;
}

void CudaWrapperF::GaussianBlurRadius(CudaWrapperF *source0, float radius)
{
	int iRadius = (int)radius;
	CudaWrapperF tempKernel(iRadius * 2 + 1, iRadius * 2 + 1);
	GenerateGaussianKernel<<<tempKernel.dimGrid, tempKernel.dimBlock>>>(tempKernel.info, radius, radius, radius + 0.5f, 255.0f);
	CudaWrapperF tempTotal(1, 1);
	assert(0);	// broken code. copy from working version.
	//tempTotal.BasicKernel1(&tempKernel, 0);	// sum all pixels
	CudaWrapperF normalizedKernel(iRadius * 2 + 1, iRadius * 2 + 1);
	normalizedKernel.BasicKernel2Image(&tempKernel, &tempTotal, 3);	// divide to normalize
	//BasicKernel1Const(&normalizedKernel, 2000, 2);
	CudaWrapperF tempBig(source0->info.width, source0->info.height);
	source0->SetAndBindTexRefSource0();
	normalizedKernel.SetAndBindTexRefSource1();
	SeperableBlurXKernelF<<<tempBig.dimGrid, tempBig.dimBlock>>>(tempBig.info, source0->info, normalizedKernel.info);
	cudaUnbindTexture(texSource1);
	cudaUnbindTexture(texSource0);
	tempBig.SetAndBindTexRefSource0();
	normalizedKernel.SetAndBindTexRefSource1();
	SeperableBlurYKernelF<<<dimGrid, dimBlock>>>(info, tempBig.info, normalizedKernel.info);
	cudaUnbindTexture(texSource1);
	cudaUnbindTexture(texSource0);
}

void CudaWrapperF::GaussianBlurFromKernel(CudaWrapperF *source0, CudaWrapperF *kernel, CudaWrapperF *tempImage)
{
	//CudaWrapperF tempBig(source0->info.width, source0->info.height);
	source0->SetAndBindTexRefSource0();
	kernel->SetAndBindTexRefSource1();
	SeperableBlurXKernelF<<<tempImage->dimGrid, tempImage->dimBlock>>>(tempImage->info, source0->info, kernel->info);
	cudaUnbindTexture(texSource1);
	cudaUnbindTexture(texSource0);
	tempImage->SetAndBindTexRefSource0();
	kernel->SetAndBindTexRefSource1();
	SeperableBlurYKernelF<<<dimGrid, dimBlock>>>(info, tempImage->info, kernel->info);
	cudaUnbindTexture(texSource1);
	cudaUnbindTexture(texSource0);
}

void CudaWrapperF::FourierForward(CudaWrapperF *source0)
{
	cufftHandle planForward;
	cufftComplex *odata;
	cufftReal *idata = source0->info.d_buffer;

	int destWidth = source0->info.width / 2 + 1;
	cudaMalloc((void **)&odata, sizeof(cufftComplex)*destWidth*source0->info.height);
	if (cudaGetLastError() != cudaSuccess){
		fprintf(stderr, "Cuda error: Failed to allocate\n");
		return;	
	}

	if (cufftPlan2d(&planForward, source0->info.width, source0->info.height, CUFFT_R2C) != CUFFT_SUCCESS){
		fprintf(stderr, "CUFFT Error: Unable to create plan\n");
		return;	
	}

	// Execute the transform out-of-place
	if (cufftExecR2C(planForward, idata, odata) != CUFFT_SUCCESS){
		fprintf(stderr, "CUFFT Error: Failed to execute plan\n");
		return;		
	}
	if (cudaThreadSynchronize() != cudaSuccess){
  		fprintf(stderr, "Cuda error: Failed to synchronize\n");
   		return;
	}

	cudaError_t err = cudaMemcpy2D((void*)info.d_buffer, info.pitchInFloats * 4,
		(const void *) odata, destWidth * 4 * 2, destWidth * 4 * 2, info.height, cudaMemcpyDeviceToDevice);

	cufftResult lamez = cufftDestroy(planForward);
	cudaFree(odata);
}

void CudaWrapperF::FourierInverse(CudaWrapperF *source0)
{
	cufftHandle planInverse;
	//cufftReal *odata;// = (cufftComplex*)info.d_buffer;
	cufftComplex *idata;// = (cufftComplex *)source0->info.d_buffer;

	int destWidth = source0->info.width / 2;
	cudaMalloc((void **)&idata, sizeof(cufftComplex)*destWidth*source0->info.height);
	if (cudaGetLastError() != cudaSuccess){
		fprintf(stderr, "Cuda error: Failed to allocate\n");
		return;	
	}

	cudaError_t err = cudaMemcpy2D((void*)idata, destWidth * 4 * 2,
		(const void *) source0->info.d_buffer, source0->info.pitchInFloats * 4, source0->info.width * 4,
		source0->info.height, cudaMemcpyDeviceToDevice);

	if (cudaGetLastError() != cudaSuccess){
		fprintf(stderr, "Cuda error: Failed to allocate\n");
		return;	
	}

	if (cufftPlan2d(&planInverse, source0->info.width - 2, source0->info.height, CUFFT_C2R) != CUFFT_SUCCESS){
		fprintf(stderr, "CUFFT Error: Unable to create plan\n");
		return;	
	}

	if (cufftExecC2R(planInverse, idata, info.d_buffer) != CUFFT_SUCCESS){
		fprintf(stderr, "CUFFT Error: Failed to execute plan\n");
		return;
	}

	if (cudaThreadSynchronize() != cudaSuccess){
  		fprintf(stderr, "Cuda error: Failed to synchronize\n");
   		return;
	}

	cufftDestroy(planInverse);
	cudaFree(idata);
}

void CudaWrapperF::ComplexSplitToPhaseMagnitude(CudaWrapperF *destPhase, CudaWrapperF *destMag)
{
	SetAndBindTexRefSource0();
	ExtractPhaseAndMagnitudeKernel<<< destPhase->dimGrid, destMag->dimBlock >>>(info, destPhase->info, destMag->info);
	cudaUnbindTexture(texSource0);
}

void CudaWrapperF::ComplexConjugate(CudaWrapperF *source0)
{
	ASSERT(source0 != this);
	source0->SetAndBindTexRefSource0();

	ComplexConjugateKernel<<< dimGrid, dimBlock >>>(info, source0->info);

	cudaUnbindTexture(texSource0);
}

void CudaWrapperF::FillRectangle(int x, int y, int width, int height, float val)
{
	dim3 dimBlockRect = dim3(16, 8, 1);
	dim3 dimGridRect = dim3(width / dimBlockRect.x + (((width % dimBlockRect.x)!=0)?1:0),
		height / dimBlockRect.y + (((height % dimBlockRect.y)!=0)?1:0), 1);

	// first clip the rectangle to screen extents.
	if (width <= 0) return;
	if (height <= 0) return;
	if (x >= info.width) return;
	if (y >= info.height) return;
	if (x + width <= 0) return;
	if (y + height <= 0) return;
	if (x < 0)
	{
		width += x;
		x = 0;
	}
	if (y < 0)
	{
		height += y;
		y = 0;
	}
	if (x + width >= info.width) width = (info.width - x) - 1;
	if (y + height >= info.height) height = (info.height - y) - 1;
	FillRectangleKernel<<< dimGridRect, dimBlockRect >>>(info, x, y, width, height, val);
}


void CudaWrapperF::FindBrightestPoint(int *pos)
{
	int *d_tempBuffer;
	cudaError_t err = cudaMalloc(&d_tempBuffer, 2*4);
	ASSERT(err != cudaErrorMemoryAllocation);
	pos[0] = INT_MIN;
	pos[1] = INT_MIN;
	cudaMemcpy(d_tempBuffer, pos, 2*4, cudaMemcpyHostToDevice);
	SetAndBindTexRefSource0();
	FindBrightestPointKernelF<<< dimGrid, dimBlock >>>(d_tempBuffer, info);
	cudaUnbindTexture(texSource0);

	//int boundingBoxResult[4];
	cudaMemcpy(pos, d_tempBuffer, 2*4, cudaMemcpyDeviceToHost);
	cudaFree(d_tempBuffer);
}


#endif //BASIC_CU
